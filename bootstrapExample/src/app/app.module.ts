import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BAlertComponent } from './b-alert/b-alert.component';
import { MyAlertComponent } from './my-alert/my-alert.component';
import { BDropdownComponent } from './b-dropdown/b-dropdown.component';
import { MyDropdownComponent } from './my-dropdown/my-dropdown.component';
import { BModalComponent } from './b-modal/b-modal.component';
import { MyModalComponent } from './my-modal/my-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    BAlertComponent,
    MyAlertComponent,
    BDropdownComponent,
    MyDropdownComponent,
    BModalComponent,
    MyModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
