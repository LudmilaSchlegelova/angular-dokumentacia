import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BDropdownComponent } from './b-dropdown.component';

describe('BDropdownComponent', () => {
  let component: BDropdownComponent;
  let fixture: ComponentFixture<BDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BDropdownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
