import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-modal',
  templateUrl: './my-modal.component.html',
  styleUrls: ['./my-modal.component.scss'],
})
export class MyModalComponent implements OnInit {
  show = true;

  constructor() {}

  ngOnInit(): void {}
}
