import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-dropdown',
  templateUrl: './my-dropdown.component.html',
  styleUrls: ['./my-dropdown.component.scss'],
})
export class MyDropdownComponent implements OnInit {
  show = true;
  constructor() {}

  ngOnInit(): void {}
}
