import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BAlertComponent } from './b-alert.component';

describe('BAlertComponent', () => {
  let component: BAlertComponent;
  let fixture: ComponentFixture<BAlertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BAlertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
