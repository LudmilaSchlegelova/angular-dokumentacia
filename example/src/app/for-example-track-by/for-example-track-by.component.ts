import { Component } from '@angular/core';

interface coursesItemType {
  id: number;
  name: string;
}

@Component({
  selector: 'for-example-track-by',
  templateUrl: './for-example-track-by.component.html',
  styleUrls: ['./for-example-track-by.component.scss'],
})
export class ForExampleTrackByComponent {
  courses: coursesItemType[];

  loadCourses() {
    this.courses = [
      { id: 0, name: 'course0' },
      { id: 1, name: 'course1' },
      { id: 2, name: 'course2' },
    ];
  }

  trackCourse(index, course) {
    return course ? course.id : undefined;
  }
}
