import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForExampleTrackByComponent } from './for-example-track-by.component';

describe('ForExampleTrackByComponent', () => {
  let component: ForExampleTrackByComponent;
  let fixture: ComponentFixture<ForExampleTrackByComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForExampleTrackByComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForExampleTrackByComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
