import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'favorite',
  templateUrl: './favorite.component.html',
  // template: `<i
  //   class="glyphicon icon"
  //   [class.glyphicon-star]="isSelected"
  //   [class.glyphicon-star-empty]="!isSelected"
  //   (click)="onClick()"
  // ></i>`,
  styleUrls: ['./favorite.component.scss'],
  styles: [
    `
      .glyphicon {
        font-size: 50px;
      }
    `,
  ],
})
export class FavoriteComponent {
  @Input('isFavorite') isSelected: boolean;
  @Output('change') click = new EventEmitter();

  onClick() {
    this.isSelected = !this.isSelected;
    this.click.emit({ newValue: this.isSelected });
  }
}
