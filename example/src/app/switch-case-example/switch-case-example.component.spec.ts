import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchCaseExampleComponent } from './switch-case-example.component';

describe('SwitchCaseExampleComponent', () => {
  let component: SwitchCaseExampleComponent;
  let fixture: ComponentFixture<SwitchCaseExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwitchCaseExampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchCaseExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
