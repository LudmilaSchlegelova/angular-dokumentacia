import { Component } from '@angular/core';

@Component({
  selector: 'switch-case-example',
  templateUrl: './switch-case-example.component.html',
  styleUrls: ['./switch-case-example.component.scss'],
})
export class SwitchCaseExampleComponent {
  viewMode: string = '';
}
