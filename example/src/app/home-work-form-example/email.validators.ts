import { AbstractControl, ValidationErrors } from '@angular/forms';

export class EmailValidator {
  static checkingCorrectValueEmail(
    control: AbstractControl
  ): Promise<ValidationErrors | null> {
    const result = new Promise((resolve) => {
      setTimeout(() => {
        if (control.value !== 'janko@gmail.com') {
          resolve({ checkingCorrectValueEmail: true });
        } else {
          resolve(null);
        }
      }, 1000);
    });
    return result;
  }
}
