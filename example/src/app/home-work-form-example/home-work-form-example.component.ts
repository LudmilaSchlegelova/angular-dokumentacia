import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from './email.validators';
import { PasswordValidator } from './password.validators';

@Component({
  selector: 'home-work-form-example',
  templateUrl: './home-work-form-example.component.html',
  styleUrls: ['./home-work-form-example.component.scss'],
})
export class HomeWorkFormExampleComponent {
  form = new FormGroup({
    //prvy parameter: '', druhy parameter: [sync funkcie], treti parameter: [async funkcie]

    email: new FormControl('', null, EmailValidator.checkingCorrectValueEmail),
    password: new FormControl(
      '',
      Validators.required,
      PasswordValidator.checkingCorrectValuePassword
    ),
  });

  login() {
    this.form.setErrors({ invalidLogin: true });
  }

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }
}

// DU2 vytvorit cely form kde bude iba e-mail a heslo, po submite sa zavola async kod (cakat 1 sec) emial a heslo musi byt rovnake janko@gmail.com (take iste heslo)  ak dam ine vypise ze taky user neexistuje, urobit to cez setError
