import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeWorkFormExampleComponent } from './home-work-form-example.component';

describe('HomeWorkFormExampleComponent', () => {
  let component: HomeWorkFormExampleComponent;
  let fixture: ComponentFixture<HomeWorkFormExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeWorkFormExampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeWorkFormExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
