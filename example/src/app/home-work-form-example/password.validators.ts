import { AbstractControl, ValidationErrors } from '@angular/forms';

export class PasswordValidator {
  static checkingCorrectValuePassword(
    control: AbstractControl
  ): Promise<ValidationErrors | null> {
    const result = new Promise((resolve) => {
      setTimeout(() => {
        if (control.value !== 'janko@gmail.com') {
          resolve({ checkingCorrectValuePassword: true });
        } else {
          resolve(null);
        }
      }, 1000);
    });
    return result;
  }
}
