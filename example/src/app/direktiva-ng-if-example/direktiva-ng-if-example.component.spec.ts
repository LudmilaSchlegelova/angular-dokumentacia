import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirektivaNgIfExampleComponent } from './direktiva-ng-if-example.component';

describe('DirektivaNgIfExampleComponent', () => {
  let component: DirektivaNgIfExampleComponent;
  let fixture: ComponentFixture<DirektivaNgIfExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirektivaNgIfExampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DirektivaNgIfExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
