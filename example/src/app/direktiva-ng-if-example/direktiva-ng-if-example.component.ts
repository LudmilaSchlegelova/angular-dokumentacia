import { Component } from '@angular/core';

@Component({
  selector: 'direktiva-ng-if-example',
  templateUrl: './direktiva-ng-if-example.component.html',
  styleUrls: ['./direktiva-ng-if-example.component.scss'],
})
export class DirektivaNgIfExampleComponent {
  courses: number[] = [];
}
