import { Component } from '@angular/core';

interface TitleType {
  id: number;
  name: string;
}

@Component({
  selector: 'radio-button-example',
  templateUrl: './radio-button-example.component.html',
  styleUrls: ['./radio-button-example.component.scss'],
})
export class RadioButtonExampleComponent {
  titles: TitleType[] = [
    { id: 1, name: 'Mr.' },
    { id: 2, name: 'Mrs.' },
    { id: 3, name: 'Miss' },
  ];
}
