import { Component, ViewEncapsulation } from '@angular/core';

// enum ViewEncapsulation {
//   Emulated = 0,
//   None = 2,
//   ShadowDom = 3,
// }

@Component({
  selector: 'encapsulation-example',
  template: `
    <h1>Hello World!</h1>
    <span class="red">Shadow DOM Rocks!</span>
  `,
  styles: [
    `
      :host {
        display: block;
        border: 1px solid black;
      }
      h1 {
        color: blue;
      }
      .red {
        background-color: red;
      }
    `,
  ],

  encapsulation: ViewEncapsulation.ShadowDom,
})
export class EncapsulationExampleComponent {}
