import { Component } from '@angular/core';

interface monthItem {
  name: string;
  year: number;
}

@Component({
  selector: 'questionnaire-example',
  templateUrl: './questionnaire-example.component.html',
  styleUrls: ['./questionnaire-example.component.scss'],
})
export class QuestionnaireExampleComponent {
  months: monthItem[] = [
    { name: 'January', year: 2020 },
    { name: 'February', year: 2020 },
    { name: 'March', year: 2020 },
    { name: 'April', year: 2020 },
    { name: 'May', year: 2020 },
    { name: 'June', year: 2020 },
    { name: 'July', year: 2020 },
    { name: 'August', year: 2020 },
    { name: 'September', year: 2020 },
    { name: 'October', year: 2020 },
    { name: 'November', year: 2020 },
    { name: 'December', year: 2020 },
  ];

  viewMode: string = 'January';
}

// DU Moja odpoved je (h1) , pouzit aspon 10 odpovedi, s tym, ze odpovede su nacitane z class komponenty, nacitanie v html je pomocou ngFor, vramci kazdeho jedneho itemu bude moznost pouzit ngSwitch. pozriet classy na switch v bootstrape. kombinacia switchu a for cyklu
