import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireExampleComponent } from './questionnaire-example.component';

describe('QuestionnaireExampleComponent', () => {
  let component: QuestionnaireExampleComponent;
  let fixture: ComponentFixture<QuestionnaireExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionnaireExampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
