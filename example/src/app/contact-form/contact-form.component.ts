import { Component } from '@angular/core';

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
})
export class ContactFormComponent {
  contactMetods = [
    { id: 1, name: 'e-mail' },
    { id: 2, name: 'phone' },
  ];

  log(x) {
    console.log(x);
  }
  submit(f) {
    console.log(f);
  }
}
