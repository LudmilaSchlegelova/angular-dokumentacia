import { Component, OnInit } from '@angular/core';
import { PhotoService } from '../photo.service';

@Component({
  selector: 'photos-component',
  templateUrl: './photos-component.component.html',
  styleUrls: ['./photos-component.component.scss'],
})
export class PhotosComponentComponent implements OnInit {
  photos: any[];

  constructor(private service: PhotoService) {}

  createPhoto(input: HTMLInputElement) {
    let photo = { title: input.value };
    input.value = '';
    this.service.createPhoto(photo).subscribe(
      (res) => {
        photo['id'] = res.json().id;
        this.photos.splice(0, 0, photo);
        console.log(res.json());
      },
      (err: Response) => {
        if (err.status === 400) {
          alert(err.json());
        } else {
          alert('Something went wrong!');
        }
      }
    );
  }

  updatePhoto(photo) {
    this.service.updatePhoto(photo).subscribe(
      (res) => {
        console.log(res.json());
      },
      (err: Response) => {
        if (err.status === 400) {
          alert(err.json());
        } else {
          alert('Something went wrong!');
        }
      }
    );
  }

  deletePhoto(photo) {
    this.service.deletePhoto(photo.id).subscribe(
      () => {
        let i = this.photos.indexOf(photo);
        this.photos.splice(i, 1);
      },
      (err: Response) => {
        if (err.status === 404) {
          alert('This photo has already been deleted!');
        } else {
          alert('Something went wrong!');
        }
      }
    );
  }

  ngOnInit() {
    this.service.getPhotos().subscribe(
      (res) => {
        this.photos = res.json();
      },
      (err) => {
        alert('Something went wrong!');
        console.log(err);
      }
    );
  }
}
