import { Component } from '@angular/core';

@Component({
  selector: 'ng-style-example',
  templateUrl: './ng-style-example.component.html',
  styleUrls: ['./ng-style-example.component.scss'],
})
export class NgStyleExampleComponent {
  canSave: boolean = false;
  handleClick() {
    this.canSave = !this.canSave;
  }
}
