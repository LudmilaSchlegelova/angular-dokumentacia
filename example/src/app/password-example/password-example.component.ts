import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PasswordValidator } from './password.validators';

@Component({
  selector: 'password-example',
  templateUrl: './password-example.component.html',
  styleUrls: ['./password-example.component.scss'],
  styles: [
    `
      form {
        margin: 20px;
      }
      .btn-reset {
        margin-bottom: 20px;
      }
      .btn-submit {
        margin-top: 20px;
      }
    `,
  ],
})
export class PasswordExampleComponent {
  showResetForm = false;
  newPasswordNotValid = false;

  form = new FormGroup({
    oldPsswd: new FormControl(
      '',
      Validators.required,
      PasswordValidator.onOldPsswd
    ),
    newPsswd1: new FormControl('', Validators.required),
    newPsswd2: new FormControl('', Validators.required),
  });

  onResetPsswd() {
    this.showResetForm = true;
  }

  get oldPsswd() {
    return this.form.get('oldPsswd');
  }

  get newPsswd1() {
    return this.form.get('newPsswd1');
  }

  get newPsswd2() {
    return this.form.get('newPsswd2');
  }

  handleChangePassword() {
    if (this.newPsswd1.value === this.newPsswd2.value) {
      // this.oldPsswd.setValue('');
      // this.newPsswd1.setValue('');
      // this.newPsswd2.setValue('');
      this.form.reset();
    } else {
      this.newPasswordNotValid = true;
    }
  }

  login() {
    // nastavenie erroru vramci validacie
    this.form.setErrors({ invalidLogin: true });
  }
}

//DU vytvorit button reset password, po kliknuti (click) sa zavola metoda, ktora prepise stav showResetForm z false na true. Prvy input bude stare heslo (1,2,3,4,5,6     validator bude async metoda po 2 sekundach ), druhy bude nove, treti bude zopakovat nove heslo, ak sa nove heslo neoveri so zopakovanym heslom vybehne alert hlaska. Ak vsetko prejde tak stare heslo sa prepise na nove heslo, vyuzit tlacidlo submit, vytvorit metodu kde prepisujem stare na nove
