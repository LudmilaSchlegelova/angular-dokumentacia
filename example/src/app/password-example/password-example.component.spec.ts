import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordExampleComponent } from './password-example.component';

describe('PasswordExampleComponent', () => {
  let component: PasswordExampleComponent;
  let fixture: ComponentFixture<PasswordExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PasswordExampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
