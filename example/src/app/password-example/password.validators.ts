import { AbstractControl, ValidationErrors } from '@angular/forms';

export class PasswordValidator {
  static onOldPsswd(
    control: AbstractControl
  ): Promise<ValidationErrors | null> {
    console.log(control.value);

    let password = '12345';
    const result = new Promise((resolve) => {
      setTimeout(() => {
        if (control.value !== password) {
          resolve({ onOldPsswd: true });
        } else {
          password = control.value;
          resolve(null);
        }
      }, 2000);
    });
    return result;
  }
}
