import { Component } from '@angular/core';

interface coursesItemType {
  id: number;
  name: string;
}

@Component({
  selector: 'ng-for-example',
  templateUrl: './ng-for-example.component.html',
  styleUrls: ['./ng-for-example.component.scss'],
})
export class NgForExampleComponent {
  courses: coursesItemType[] = [
    { id: 0, name: 'course0' },
    { id: 1, name: 'course1' },
    { id: 2, name: 'course2' },
  ];

  onAdd() {
    const coursesLength = this.courses.length;
    this.courses.push({
      id: coursesLength,
      name: 'course' + coursesLength,
    });
  }
  onRemove(course) {
    let index = this.courses.indexOf(course);
    this.courses.splice(index, 1);
  }
  onChange(course) {
    course.name = 'updated';
  }
}
