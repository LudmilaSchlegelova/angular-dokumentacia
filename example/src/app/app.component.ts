import { Component } from '@angular/core';

interface favoriteChangedEventArgs {
  newValue: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  post = { isFavorite: true };
  onFavoriteChanged(eventArgs: favoriteChangedEventArgs) {
    console.log('favorite changed: ', eventArgs.newValue);
  }
}
