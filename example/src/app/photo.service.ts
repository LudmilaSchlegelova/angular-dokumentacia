import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class PhotoService {
  private url: string = 'http://jsonplaceholder.typicode.com/photos';

  constructor(private http: Http) {}

  getPhotos() {
    return this.http.get(this.url);
  }

  createPhoto(photo) {
    return this.http.post(this.url, JSON.stringify(photo));
  }

  updatePhoto(photo) {
    return this.http.patch(
      this.url + '/' + photo.id,
      JSON.stringify({ isRead: true })
    );
  }

  deletePhoto(id) {
    return this.http.delete(this.url + '/' + id);
  }
}
