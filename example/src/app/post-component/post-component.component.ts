import { Component, OnInit } from '@angular/core';
import { PostService } from '../post.service';

@Component({
  selector: 'post-component',
  templateUrl: './post-component.component.html',
  styleUrls: ['./post-component.component.scss'],
})
export class PostComponentComponent implements OnInit {
  posts: any[];

  constructor(private service: PostService) {}

  createPost(input: HTMLInputElement) {
    let post = { title: input.value };
    input.value = '';
    this.service.cratePost(post).subscribe(
      (response) => {
        post['id'] = response.json().id;
        this.posts.splice(0, 0, post);
        console.log(response.json());
      },
      (error: Response) => {
        if (error.status === 400) {
          alert(error.json());
        } else {
          alert('An unexpected error occurred!');
        }
      }
    );
  }

  updatePost(post) {
    this.service.updatePost(post).subscribe(
      (response) => {
        console.log(response.json());
      },
      (error: Response) => {
        if (error.status === 400) {
          alert(error.json());
        } else {
          alert('An unexpected error occurred!');
        }
      }
    );
  }

  deletePost(post) {
    this.service.deletePost(post.id).subscribe(
      () => {
        let index = this.posts.indexOf(post);
        this.posts.splice(index, 1);
      },
      (error: Response) => {
        if (error.status === 404) {
          alert('This post has already been deleted!');
        } else {
          alert('An unexpected error occurred!');
        }
      }
    );
  }

  ngOnInit() {
    this.service.getPosts().subscribe(
      (response) => {
        this.posts = response.json();
      },
      (error) => {
        alert('An unexpected error occurred!');
        console.log(error);
      }
    );
  }
}
