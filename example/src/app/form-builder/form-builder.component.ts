import { Component } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss'],
})
export class FormBuilderComponent {
  //toto je to iste ako zapis dole:
  // form = new FormGroup({
  //   name: new FormControl('', Validators.required),
  //   contact: new FormGroup({
  //     email: new FormControl(),
  //     phone: new FormControl(),
  //   }),
  //   topics: new FormArray([]),
  // });

  //zapis formBuilderu
  addressForm: FormGroup;
  saveData: any;

  constructor(private fb: FormBuilder) {
    this.addressForm = fb.group({
      addressList: fb.array([]),
    });
    this.addAddress();
  }

  get addresses() {
    return this.addressForm.get('addressList') as FormArray;
  }

  addAddress() {
    this.addresses.push(this.createAddress());
  }

  createAddress() {
    return this.fb.group({ streetAddress: [], city: [], state: [], zip: [] });
  }

  save() {
    this.saveData = this.addressForm.value;
  }

  removeAddressAt(i) {
    this.addresses.removeAt(i);
    this.saveData.addressList.splice(i, 1);
  }
}
