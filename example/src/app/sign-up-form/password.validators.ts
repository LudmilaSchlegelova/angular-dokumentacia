import { AbstractControl, ValidationErrors } from '@angular/forms';

export class UserPasswordValidator {
  static mustIncludes(control: AbstractControl): ValidationErrors | null {
    const pattern = new RegExp(
      '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]{2})[0-9a-zA-Z]{3,10}'
    );

    if (!pattern.test(control.value)) {
      return { mustIncludes: true };
    }
    return null;
  }
  static mustBeValue(
    control: AbstractControl
  ): Promise<ValidationErrors | null> {
    const result = new Promise((resolve) => {
      setTimeout(() => {
        console.log(control.value);
        if (control.value !== '12Ab34') {
          resolve({ mustBeValue: true });
        } else {
          resolve(null);
        }
      }, 500);
    });
    console.log(result);

    return result;
  }
}
