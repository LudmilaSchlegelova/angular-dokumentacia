import { AbstractControl, ValidationErrors } from '@angular/forms';

export class UserNameValidators {
  static cannotContainSpace(control: AbstractControl): ValidationErrors | null {
    if ((control.value as string).indexOf(' ') >= 0) {
      return { cannotContainSpace: true };
      // return {
      //   minlength: { requiredLength: 10, actualLength: control.value.length },
      // };
    }
    return null;
  }
  static shouldBeJanko(
    control: AbstractControl
  ): Promise<ValidationErrors | null> {
    const result = new Promise((resolve) => {
      setTimeout(() => {
        console.log(control.value);

        if (control.value === 'janko') {
          resolve({ shouldBeJanko: true });
        } else {
          resolve(null);
        }
      }, 500);
    });
    console.log(result);

    return result;
  }
}
