import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserPasswordValidator } from './password.validators';
import { UserNameValidators } from './username.validators';

@Component({
  selector: 'sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.scss'],
})
export class SignUpFormComponent {
  form = new FormGroup({
    //prvy parameter: '', druhy parameter: [sync funkcie], treti parameter: [async funkcie]
    account: new FormGroup({
      username: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(3),
          UserNameValidators.cannotContainSpace,
        ],
        UserNameValidators.shouldBeJanko
      ),
      password: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(10),
          UserPasswordValidator.mustIncludes,
        ],
        UserPasswordValidator.mustBeValue
      ),
    }),
  });

  login() {
    // let isValid = authService.login(this.form.value);
    this.form.setErrors({ invalidLogin: true });
  }

  get username() {
    return this.form.get('account.username');
  }
  get password() {
    return this.form.get('account.password');
  }
}

//https://stackoverflow.com/questions/52363204/angular-6-settimeout-function-is-not-working-in-my-custom-validator
