import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippingBillingDetailsExampleComponent } from './shipping-billing-details-example.component';

describe('ShippingBillingDetailsExampleComponent', () => {
  let component: ShippingBillingDetailsExampleComponent;
  let fixture: ComponentFixture<ShippingBillingDetailsExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShippingBillingDetailsExampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingBillingDetailsExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
