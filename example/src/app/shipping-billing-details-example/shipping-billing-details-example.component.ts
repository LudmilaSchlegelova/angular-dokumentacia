import { Component } from '@angular/core';

@Component({
  selector: 'shipping-billing-details-example',
  templateUrl: './shipping-billing-details-example.component.html',
})
export class ShippingBillingDetailsExampleComponent {
  showElement: boolean = false;

  onClick() {
    this.showElement = !this.showElement;
    console.log('ok');
  }
}
