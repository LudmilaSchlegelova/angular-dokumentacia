import { Component } from '@angular/core';

@Component({
  selector: 'hidden-example',
  templateUrl: './hidden-example.component.html',
  styleUrls: ['./hidden-example.component.scss'],
})
export class HiddenExampleComponent {
  courses: number[] = [];
}
