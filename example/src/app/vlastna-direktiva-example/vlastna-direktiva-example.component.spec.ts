import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VlastnaDirektivaExampleComponent } from './vlastna-direktiva-example.component';

describe('VlastnaDirektivaExampleComponent', () => {
  let component: VlastnaDirektivaExampleComponent;
  let fixture: ComponentFixture<VlastnaDirektivaExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VlastnaDirektivaExampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VlastnaDirektivaExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
