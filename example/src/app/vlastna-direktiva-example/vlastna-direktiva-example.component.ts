import { Directive, ElementRef, HostListener } from '@angular/core';

//Renderer2 https://angular.io/api/core/Renderer2
// setStyle(el: any, style: string, value: any, flags?: RendererStyleFlags2): void --> Implement this callback to set a CSS style for an element in the DOM.

//Angular ElementRef is a wrapper around a native DOM element (HTML element) object. It contains the property nativeElement, which holds the reference to the underlying DOM object. We can use it to manipulate the DOM.

@Directive({
  selector: '[myInput]',
})
export class VlastnaDirektivaExampleComponent {
  constructor(private el: ElementRef) {}

  @HostListener('focus') onFocus() {
    this.el.nativeElement.style.border = '2px solid red';
    console.log('onFocus', this.el);
  }
  @HostListener('blur')
  onBlur() {
    this.el.nativeElement.style.border = '2px solid black';
    console.log('onBlur');
  }
}

//  DU2 urobit custom directivu vlastnu, bude to input. Ked ho focusnem prida sa mu cerveny border, ked bude onBlur tak border sa odstrani
