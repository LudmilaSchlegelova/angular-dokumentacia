import { Component } from '@angular/core';

interface isVisibleType {
  message1: boolean;
  message2: boolean;
  message3: boolean;
}

@Component({
  selector: 'accordion-example',
  templateUrl: './accordion-example.component.html',
  styles: [
    `
      .accordion-container,
      .accordion-wrapper {
        border: 1px solid rgb(224, 213, 213);
        border-radius: 5px;
        width: 50%;
        padding: 20px;
      }

      .accordion-wrapper {
        width: 100%;
        background-color: rgba(0, 0, 0, 0.03);
        padding: 0;
      }

      .accordion-div {
        padding: 20px;
        list-style-type: none;
      }

      .simple-div,
      .fancy-div,
      .disabled-div {
        color: #007bff;
        border-bottom: 1px solid rgb(224, 213, 213);
      }

      .accordion-btn {
        border-color: transparent;
        background-color: transparent;
        cursor: pointer;
        padding: 6px 12px;
      }

      .accordion-btn:focus {
        text-decoration: underline;
        outline: 0;
        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
        border-radius: 5px;
      }

      .accordion-btn:disabled {
        cursor: default;
        color: #6c757d;
      }

      .fancy-p,
      .simple-p,
      .disabled-p {
        border-bottom: 1px solid rgb(224, 213, 213);
        background-color: white;
        padding: 20px;
      }
    `,
  ],
})
export class AccordionExampleComponent {
  isVisible: isVisibleType = {
    message1: false,
    message2: false,
    message3: false,
  };
  message: string = `
  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
      richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
      dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
      moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
      assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
      wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
      butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
      aesthetic synth nesciunt you probably haven't heard of them accusamus
      labore sustainable VHS.
  `;

  onClick(value) {
    switch (true) {
      case value === 1:
        this.isVisible.message1 = !this.isVisible.message1;
        break;
      case value === 2:
        this.isVisible.message2 = !this.isVisible.message2;
        break;
      case value === 3:
        this.isVisible.message3 = !this.isVisible.message3;
        break;
    }
  }
}

//DU2 https://ng-bootstrap.github.io/#/components/accordion/examples pouzit ngIf, meni sa stav z true na false
