import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FavoriteComponent } from './favorite/favorite.component';
import { PanelComponent } from './panel/panel.component';
import { EncapsulationExampleComponent } from './encapsulation-example/encapsulation-example.component';
import { DirektivaNgIfExampleComponent } from './direktiva-ng-if-example/direktiva-ng-if-example.component';
import { HiddenExampleComponent } from './hidden-example/hidden-example.component';
import { SwitchCaseExampleComponent } from './switch-case-example/switch-case-example.component';
import { NgForExampleComponent } from './ng-for-example/ng-for-example.component';
import { QuestionnaireExampleComponent } from './questionnaire-example/questionnaire-example.component';
import { ForExampleTrackByComponent } from './for-example-track-by/for-example-track-by.component';
import { NgStyleExampleComponent } from './ng-style-example/ng-style-example.component';
import { CustomDirectivaExampleComponent } from './custom-directiva-example/custom-directiva-example.component';
import { RatingExampleComponent } from './rating-example/rating-example.component';
import { AccordionExampleComponent } from './accordion-example/accordion-example.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShippingBillingDetailsExampleComponent } from './shipping-billing-details-example/shipping-billing-details-example.component';
import { VlastnaDirektivaExampleComponent } from './vlastna-direktiva-example/vlastna-direktiva-example.component';
import { RadioButtonExampleComponent } from './radio-button-example/radio-button-example.component';
import { SignUpFormComponent } from './sign-up-form/sign-up-form.component';
import { NewCourseFormComponent } from './new-course-form/new-course-form.component';
import { FormBuilderComponent } from './form-builder/form-builder.component';
import { MyFormComponent } from './my-form/my-form.component';
import { HomeWorkFormExampleComponent } from './home-work-form-example/home-work-form-example.component';
import { PasswordExampleComponent } from './password-example/password-example.component';
import { PostComponentComponent } from './post-component/post-component.component';
import { PostService } from './post.service';
import { PhotosComponentComponent } from './photos-component/photos-component.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { GithubProfileComponentComponent } from './github-profile-component/github-profile-component.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    FavoriteComponent,
    PanelComponent,
    EncapsulationExampleComponent,
    DirektivaNgIfExampleComponent,
    HiddenExampleComponent,
    SwitchCaseExampleComponent,
    NgForExampleComponent,
    QuestionnaireExampleComponent,
    ForExampleTrackByComponent,
    NgStyleExampleComponent,
    CustomDirectivaExampleComponent,
    RatingExampleComponent,
    AccordionExampleComponent,
    ContactFormComponent,
    ShippingBillingDetailsExampleComponent,
    VlastnaDirektivaExampleComponent,
    RadioButtonExampleComponent,
    SignUpFormComponent,
    NewCourseFormComponent,
    FormBuilderComponent,
    MyFormComponent,
    HomeWorkFormExampleComponent,
    PasswordExampleComponent,
    PostComponentComponent,
    PhotosComponentComponent,
    HomeComponent,
    GithubProfileComponentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/photos', pathMatch: 'full' },
      { path: 'photos', component: PhotosComponentComponent },
      { path: 'profile/:username', component: GithubProfileComponentComponent },
      { path: '**', component: NotFoundComponent },
    ]),
  ],
  providers: [PostService],
  bootstrap: [AppComponent],
})
export class AppModule {}
