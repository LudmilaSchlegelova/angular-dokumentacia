import { Component } from '@angular/core';

@Component({
  selector: 'rating-example',
  templateUrl: './rating-example.component.html',
  styleUrls: ['./rating-example.component.scss'],
})
export class RatingExampleComponent {
  rateCount: number = 0;
  stars: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  onClick(index) {
    this.rateCount = index;
  }
}

// DU https://ng-bootstrap.github.io/#/components/rating/examples
// pouzit ngFor, ngStyle, ngClass, click(), dve premenne rateCount a druha stars []
