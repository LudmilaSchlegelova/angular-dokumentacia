import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-github-profile-component',
  templateUrl: './github-profile-component.component.html',
  styleUrls: ['./github-profile-component.component.scss'],
})
export class GithubProfileComponentComponent implements OnInit {
  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => console.log(params));
    console.log(this.route.snapshot.paramMap.get('username'));
  }
}
