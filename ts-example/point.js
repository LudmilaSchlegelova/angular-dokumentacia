"use strict";
exports.__esModule = true;
exports.Position = void 0;
var Position = /** @class */ (function () {
    function Position(x, y) {
        var _this = this;
        this.x = x;
        this.y = y;
        this.draw = function () {
            console.log('X: ' + _this.x + ', Y: ' + _this.y);
        };
    }
    return Position;
}());
exports.Position = Position;
