"use strict";
exports.__esModule = true;
var point_1 = require("./point");
var point = new point_1.Position(1, 2);
point.draw();
//DU vyrobit novy subor domaca1. vyrobit a vyexportovat classu s nazvom likeComponent v subore like.Component.ts. bude tu definovany constructor s 2 vstup.hodnotami likesCount: number a isSelected: boolean
//v classe bude definovana metoda onClick(), bude v nej logika => ak isSelected je true tak sa z hodnoty likesCount odpocita 1, opacne ak podmienka nebude splnena k likesCount sa pripocita 1
// vramci funkcie onClick pod if else logikou urobit druhy sposob zapisu pomocou otaznika a dvojbodky
//pod touto logikou vykonavam negaciu, prepisujem isSelected na negaciu hodnoty isSelected
//vramci domaca1 si vyrobim dalsi subor main.ts , importnem si sem likeComponent a vytiahnem classu likeComponent. pod to si vyrobim instanciu tejto classy, ktoru vlozim do prepisovatelnej premennej s nazvom component. Vramci instancie definujem dve vstup. prem. (10,true)
//z instancie component si vytiahnem onClick()
//pod to vykonzolujem string `likesCount: ${component.likesCount}, isSelected: ${component.isSelected}`
//pre spustenie pisat do terminalu tsc *.ts && node main.js
//vysledok ma byt likesCount: 11, isSelected: false
