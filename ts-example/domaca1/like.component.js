"use strict";
exports.__esModule = true;
exports.LikeComponent = void 0;
var LikeComponent = /** @class */ (function () {
    function LikeComponent(likesCount, isSelected) {
        var _this = this;
        this.likesCount = likesCount;
        this.isSelected = isSelected;
        this.onClick = function () {
            if (_this.isSelected) {
                _this.likesCount++;
            }
            else {
                _this.likesCount--;
            }
            _this.likesCount = _this.likesCount;
            // this.likesCount += this.isSelected ? 1 : -1
            // this.isSelected === true ? (this.likesCount -= 1) : (this.likesCount += 1)
            _this.isSelected = !_this.isSelected;
        };
    }
    return LikeComponent;
}());
exports.LikeComponent = LikeComponent;
