# angular-dokumentacia

Globalna instalacia angular cli

```
npm install -g @angular/cli
```

[Link](https://cli.angular.io/)

V ramci angularu pouzivame TypeScript. Je to nadstavba JavaScriptu s definovanymi typmi premennych.

Instalacia

```
npm install -g typescript
```

## TypeScript

typovanie premennych:

```ts
let a: number = 1
let b: boolean = true
let c: string = ''
let d: any = '' || true || 1 // hodnota kazdeho typu
let e: number[] = [1, 2, 3]
let f: any[] = [1, true, '']

//string
const food: string = 'Bread'
//number
const count: number = 33
//boolean
const state: boolean = true
//array of numbers
const array1: number[] = [0, 2, 4]
//array of strings
const array2: string[] = ['0', '2', '4']
//object of numbers
let object1: { value1: number; value2: number; value3: number }
object1 = { value1: 1, value2: 2, value3: 3 }
//object of strings
let object2: { value1: string; value2: string; value3: string }
object2 = { value1: '0', value2: '2', value3: '4' }
let value: string || number
value = 'bla'
value = 13
//color je nepovinna, ak neuvediem ziadnu color, defaltne bude red
const draw = (color?: string = 'red'): void => console.log(color);
const draw2 = (color?: string = 'red'): string => color;
//null
const item1: null = null
//undefined
const item2: undefined = undefined

const colorRed = 0
const colorGreen = 1
const colorBlue = 2

enum Color {
	Red = 0,
	Green = 1,
	Blue = 2,
}
let backgroundColor = Color.Red
```

Vramci TypeScriptu vieme definovat interface pre nejaku vstupnu premennu ako objekt alebo pole

```ts
interface Point {
	x: number
	y: number
	z: number
}

const drawPoint = (point: Point) => {
	console.log(point)
}

drawPoint({ x: 1, y: 2, z: 3 })
```

Ak typujem funkciu v TypeScripte bud definujem navratovu hodnotu tej funkcie, alebo
ak viem, ze funkcia nevrati nic pri volani definujem jej typ ako `void`

```ts
interface ComponentProps {
	color: string
	className: string
	value: number
	onClick: (selector: string) => void
}
```

```ts
interface ComponentProps {
	renderChildren: (children: JSX) => JSX[]
}
```

Zapis classy v TypeScripte

```ts
class Position {
	constructor(public x?: number, private y?: number) {}
	public draw = () => {
		console.log('X: ' + this.x + ', Y: ' + this.y)
	}
}

let point = new Position(1, 2)
console.log(point.x)

// point.x = 1
// point.y = 2
point.draw()
```

Viacero metod v classe s private a public definiciou, defaltne je vsetko public

```ts
class Position {
	constructor(private x?: number, private y?: number) {}
	public draw = () => {
		console.log('X: ' + this.x + ', Y: ' + this.y)
	}
	getX = () => {
		return this.x
	}

	getY = () => this.y //i tak še da

	setX = (value: number) => {
		if (value < 0) {
			throw new Error('Value cannot be less than 0!')
		}
		this.x = value
	}
}

let point = new Position(1, 2)
console.log(point.getX())
point.setX(10)

// point.x = 1
// point.y = 2
point.draw()
```

## Angular

- Je to framework, ktory umoznuje vytvarat jednostrankove reaktivne aplikacie. Prva verzia Angularu sa nazyvala `AngularJS(Angular 1)`, nasledne tato verzia bola prepisana uplne odznova na verziu `Angular 2`, potom nasledoval update na `Angular 4` (`Angular 3` verzia bola zrusena). Ostatne verzie: `Angular 5,6,7,8,9,10`. Aktualne uz je `Angular 11`, ale este to nie je stabilna verzia. Nove verzie sa vydavaju kazdych 6 mesiacov.

### Zakladne nastavenie projektu pomocou Bootstrapu

- Najprv potrebujeme nainstalovat Bootstrap pomocou prikazu

Stiahnutie konkretnej verzie

```
npm i --save bootstrap@3
```

Stiahnutie poslednej verzie

```
npm i --save bootstrap
```

Po instalacii je nutne pridat schopnost bootstrapu v subore `angular.json`, konkretne vramci `build` objektu v options.

![alt](./doc-image/Screenshot_1.png)

```
"node_modules/bootstrap/dist/css/bootstrap.min.css"
```

Alebo staci vramci `angular.json` definovat subor style.scss a v nom priamo definovat import bootstrapu.

```css
@import '~bootstrap/dist/css/bootstrap.min.css';
```

### Data binding komunikacia

- Najprv Angular skompiluje TypeScript do JavaScriptu a nasledne sa preposlu premenne z JavaScriptu do Htmlka. Ak je vyvolany nejaky event, tak sa prevola definovana metoda s jej logikou. Ked sa zmeni stav, prekresli sa len ta dana komponenta, kde nastala nejaka zmena.

### components

definicia componenty

```ts
import { Component } from '@angular/core'
@Component({
	selector: 'courses', // tu definujeme nazov tejto componenty a tento nazov sa pouziva ako htmlkove volanie v inej componente (pouzitie tejto componenty je <courses />)
	template: '<h2>Courses</h2>',
})
export class CoursesComponent {}
```

pouzitie componenty

```tsx
<h1>Angular</h1>
<courses></courses>
```

Automaticke generovanie componenty sa vykona pomocou prikazu

```
ng generate component nazovMojejComponenty
```

skrateny prikaz

```
ng g c nazovMojejComponenty
```

### Angular templates

- vkladanie premennej vramci templates
- template je kusok htmlka a vramci tohto htmlka mozme vyuzivat mnoho angular funkcii/schopnosti

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses', // tu definujeme nazov tejto componenty a tento nazov sa pouziva ako htmlkove volanie v inej componente (pouzitie tejto componenty je <courses />)
	// template: '<h2>title: {{ title }}</h2>',
	//ALEBO
	template: '<h2>{{"title: " + title }}</h2>',
})
export class CoursesComponent {
	title = 'List of courses'
}
```

- okrem premennych vieme v templates volat aj funkcie

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses', // tu definujeme nazov tejto componenty a tento nazov sa pouziva ako htmlkove volanie v inej componente (pouzitie tejto componenty je <courses />)
	template: '<h2>{{getTitle()}}</h2>',
})
export class CoursesComponent {
	title = 'List of courses'
	getTitle() {
		return this.title
	}
}
```

### Direktiva

Rozlisujeme `Attribute Directives` a `Structural Directives`:

- Atribut direktiva meni vzhlad alebo spravanie iba toho prvku v DOMe, do ktoreho bola pridana. Vyzera ako normalny html atribut, ktory moze obsahovat databinging alebo event binding
- Strukturalna direktiva meni vzhlad a spravanie celej casti v DOMe (nie len jedneho daneho prvku), napriklad sa elementy pridavaju alebo odoberaju. Vyzera ako normalny html atribut, ale na jeho zaciatok pridavame `*`. Dolezite je povedat, ze `nemozeme mat viac ako jednu strukturalnu direktivu v elemente`.

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses',
	template: `<h2>{{ title }}</h2>
		<ul>
			<li *ngFor="let course of courses">{{ course }}</li>
		</ul>`,
})
export class CoursesComponent {
	title = 'List of courses'
	courses = ['course1', 'course2', 'course3']
}
```

### Direktiva, Renderer, @HostListener(), @HostBindning()

- `Renderer` sa pouziva na menenie vzhladu html elementov. Viac info: [link](https://angular.io/api/core/Renderer2)
- `@HostListener()` dekorator sa pouziva na tvorbu posluchaca (addEventListener v nativnom JS).

Napriklad ak chceme menit vzhlad elementu pri nejakom evente s pouzitim metody, v tomto pripade mame event `mouseenter` a metodu `mouseover`:

```tsx
	@HostListener('mouseenter') mouseover(eventData: Event) {
		this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue')
	}
```

- `@HostBinding()` dekorator sa pouziva ak chceme menit vzhlad elementu bez pouzitia rendereru. Do zatvoriek uvadzame, ktoru vlastnost (property) daneho elementu chceme menit, v tomto pripade menime `backgroundColor` pomocou atributu `backgroundColor` :

```tsx
 @HostBinding("style.backgroundColor") backgroundColor: string;
```

```html
<li
	class="list-group-item"
	[ngClass]="{ odd: even % 2 !== 0 }"
	[ngStyle]="{
              backgroundColor: even % 2 !== 0 ? 'yellow' : 'transparent'
            }"
	*ngFor="let even of evenNumbers"
>
	{{ even }}
</li>
```

### Host element

Pouzitie komponenty v rodicovi sa moze nazyvat ako host element pre htmlkovy tag h1

```tsx
selector: 'my-comp'
template: `
<h1> some heading </h1>
`


<my-comp></my-comp>  // my-comp is the host element of h1
```

### @HostListener() vs. @HostBinding()

Rozdiel medzi tymito dekoratormi je v tom, ze dekorator `@HostBinding()` meni vzhlad host elementu pomocou vlastnosti, ktore upresnime v zatvorke dekoratora a dekorator `@HostListener()` dokaze menit vzhlad host elementu pri vyvolani nejakeho eventu (ako je napriklad event click, mouseleave,...)

#### Priklad na Direktiva, Renderer a @HostListener():

(subor app.component.html)

```html
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-primary" (click)="onlyOdd = !onlyOdd">
				Only show odd numbers
			</button>
			<br /><br />
			<ul class="list-group">
				<div *ngIf="onlyOdd">
					<li
						class="list-group-item"
						[ngClass]="{ oddClass: odd % 2 !== 0 }"
						[ngStyle]="{
              backgroundColor: odd % 2 !== 0 ? 'yellow' : 'transparent'
            }"
						*ngFor="let odd of oddNumbers"
					>
						{{ odd }}
					</li>
				</div>
				<div *ngIf="!onlyOdd">
					<li
						class="list-group-item"
						[ngClass]="{ odd: even % 2 !== 0 }"
						[ngStyle]="{
              backgroundColor: even % 2 !== 0 ? 'yellow' : 'transparent'
            }"
						*ngFor="let even of evenNumbers"
					>
						{{ even }}
					</li>
				</div>
			</ul>
			<p appBasicHighlight>Style me with basic directive!</p>
			<p appBetterHighlight>Style me with a better directive!</p>
		</div>
	</div>
</div>
```

(subor app.component.ts)

```ts
import { Component } from '@angular/core'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
})
export class AppComponent {
	numbers = [1, 2, 3, 4, 5]
	oddNumbers = [1, 3, 5]
	evenNumbers = [2, 4]
	onlyOdd = false
}
```

subor better-highlight.directive.ts => Tento subor (direktivu) sme si vyrobili pomocou prikazu:

```
ng g d nazovDirektivy
```

```tsx
import {
	Directive,
	ElementRef,
	HostListener,
	OnInit,
	Renderer2,
} from '@angular/core'

@Directive({
	selector: '[appBetterHighlight]', // tym, ze selektor mame v hranatej zatvorke nebude sa selektovat ako element ale ako atribut. Preto, ked tento selektor pouzijeme v subore app.component.html v paragrafoch, tak ho uz nemusime davat do hranatej zatvorky
})
export class BetterHighlightDirective implements OnInit {
	constructor(private elRef: ElementRef, private renderer: Renderer2) {}

	ngOnInit() {}

	@HostListener('mouseenter') mouseover(eventData: Event) {
		this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue')
	}

	@HostListener('mouseleave') mouseleave(eventData: Event) {
		this.renderer.setStyle(
			this.elRef.nativeElement,
			'background-color',
			'transparent'
		)
	}
}
```

### Dropdown Directive priklad

Musime si vytvorit direktivu pomocou

```
ng g d nazovDirektivy
```

(subor dropdown.direktive.ts)

```tsx
import { Directive, HostBinding, HostListener } from '@angular/core'

@Directive({
	selector: '[appDropdown]',
})
export class DropdownDirective {
	@HostBinding('class.open') isOpen = false // pomocou dekoratora pridavame classu open

	@HostListener('click') toogleOpen() {
		//dropdown reaguje na click event
		this.isOpen = !this.isOpen //menime stav z false na true a naopak
	}
}
```

Kedze v header mame dropdown menu, musime v tejto komponente pridat direktivu `appDropdown` na miesto, ktore v sebe obsahuje dropdown:

(subor header.component.html)

```html
<ul class="nav navbar-nav navbar-right">
	<li class="dropdown" appDropdown>
		<a href="#" class="dropdown-toggle" role="button"
			>Manage <span class="caret"></span
		></a>
		<ul class="dropdown-menu">
			<li><a href="#">Save Data</a></li>
			<li><a href="#">Fetch Data</a></li>
		</ul>
	</li>
</ul>
```

Tak isto direktivu `appDropdown` musime pridat do komponenty recipe-detail.component.html, pretoze tam sa tiez nachadza dropdown menu:

(subor recipe-detail.component.html)

```html
<div class="row">
	<div class="col-xs-12">
		<div class="btn-group" appDropdown>
			<button type="button" class="btn btn-primary dropdown-toggle">
				Manage Recipe <span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<li><a href="#">To Shopping List</a></li>
				<li><a href="#">Edit Recipe</a></li>
				<li><a href="#">Delete Recipe</a></li>
			</ul>
		</div>
	</div>
</div>
```

Ak chceme mat moznost zatvarat dropdown menu kliknutim nie len na dany element ale pomocou kliknutia kdekolvek vramci nasej aplikacie, kod vieme zmenit nasledovne:

(subor dropdown.direktive.ts)

```tsx
import { Directive, ElementRef, HostBinding, HostListener } from '@angular/core'

@Directive({
	selector: '[appDropdown]',
})
export class DropdownDirective {
	@HostBinding('class.open') isOpen = false
	@HostListener('document:click', ['$event']) toggleOpen(event: Event) {
		//neklikame na dropdown ale na document
		this.isOpen = this.elRef.nativeElement.contains(event.target)
			? !this.isOpen
			: false
	}
	constructor(private elRef: ElementRef) {}
}
```

### Services

- Servisa sluzi na pridavanie logiky pre volanie http requestov
- Nazov suboru sa zapisuje 'nazov.service.ts'

Generovanie servicy pomocou prikazu

```
ng g s nazovMojejServicy
```

- Bud ju zapiseme do app.module.ts do `providers` alebo to urobime takto:

```tsx
@Injectable({ providedIn: 'root' })
export class UserService {}
```

```tsx
export class CoursesService {
	getCourses() {
		return ['course1', 'course2', 'course3']
	}
}
```

```tsx
import { Component } from '@angular/core'
import { CoursesService } from './courses.service'
@Component({
	selector: 'courses',
	template: `<h2>{{ title }}</h2>
		<ul>
			<li *ngFor="let course of courses">{{ course }}</li>
		</ul>`,
})
export class CoursesComponent {
	title = 'List of courses'
	courses

	constructor(service: CoursesService) {
		// let service = new CoursesService();
		this.courses = service.getCourses()
	}
}
```

```tsx
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { CoursesComponent } from './courses.component'
import { CourseComponent } from './course/course.component'
import { CoursesService } from './courses.service'

@NgModule({
	declarations: [AppComponent, CoursesComponent, CourseComponent],
	imports: [BrowserModule, AppRoutingModule],
	providers: [CoursesService], //definovanie servicy, z ktorej vyrobim jednu instanciu
	bootstrap: [AppComponent],
})
export class AppModule {}
```

### Hierarchicke usporiadanie

- Najvyssi level `AppModule` - ak pridame servicu do pola `providers` v subore app.module.ts, bude dostupna pre celu aplikaciu
- Stredny level `AppComponent` - ak pridame servicu do pola `providers` v subore app.component.ts, bude dostupna pre nu a vsetky jej deti. To plati aj pre ine komponenty (nie len pre app.component.ts), ak v nich bude servica uvedena, tak bude dostupna aj pre vsetky deti danej komponenty. Dolezite je ale povedat, ze nebude dostupna pre app.component.ts, pretoze `dostupnost sa vztahuje len na deti, nie na rodicov`
- Najnizsi level `ostatne komponenty` - ak pridame servicu do pola `providers` v komponente, ktora nema ziadne deti, servica bude dostupna len pre tuto danu komponentu. Dolezite je poznamenat, ze takto definovana servica prepisuje servicy, ktore su definovane na vyssej urovni.

### Vyuzivanie servicy v service

- Ak chceme servicu pouzit v inej service ci v komponente, ta `musi obsahovat` pripojene `meta data`. Meta data su ku komponente pripojene cez `@Component`, direktiva ma meta data pripojene pomocou `@Directive`. Servica ale nema meta data pripojene cez `@Service`, `nieco take neexistuje`. V pripade service sa meta data pripajaju pomocou `@Injectable`. @Injectable nepridavame do servicy, ktoru chceme pouzit, ale `na miesto kde ju chceme pouzit`.

- Ak danu servicu budeme pouzivat v projekte, ale v service nechceme pouzit ziadnu inu servicu, tak `@Injectable` nepridavame. (V novsich verziach Angularu je vsak odporucane aj v takychto pripadoch pridavat dekorator @Injectable())
  Cize takato service bude vyzerat nasledovne:

```tsx
export class LoggingService {
	logStatusChange(status: string) {
		console.log('A server status changed, new status: ' + status)
	}
}
```

- Ak vsak v service budeme vyuzivat aj inu servicu, tak bude vyzerat nasledovne:
  Nesmieme zabudnut na import:

```
import { Injectable } from "@angular/core"
```

```tsx
import { Injectable } from '@angular/core'
import { LoggingService } from './logging.service'

@Injectable()
export class AccountsService {
	accounts = [
		{
			name: 'Master Account',
			status: 'active',
		},
		{
			name: 'Testaccount',
			status: 'inactive',
		},
		{
			name: 'Hidden Account',
			status: 'unknown',
		},
	]

	constructor(private loggingServis: LoggingService) {}

	addAccount(name: string, status: string) {
		this.accounts.push({ name: name, status: status })
	}
	updateAccount(id: number, status: string) {
		this.accounts[id].status = status
	}
}
```

### Vyuzivanie servisy pri medzikomponentovej komunikacii

(subor account.service.ts)

```tsx
import { Injectable, EventEmitter } from '@angular/core'
import { LoggingService } from './logging.service'

@Injectable()
export class AccountsService {
	accounts = [
		{
			name: 'Master Account',
			status: 'active',
		},
		{
			name: 'Testaccount',
			status: 'inactive',
		},
		{
			name: 'Hidden Account',
			status: 'unknown',
		},
	]

	statusUpdated = new EventEmitter<string>() //vytvarame novu instatnciu

	constructor(private loggingServis: LoggingService) {}

	addAccount(name: string, status: string) {
		this.accounts.push({ name: name, status: status })
	}
	updateAccount(id: number, status: string) {
		this.accounts[id].status = status
	}
}
```

(subor account.component.ts)

```tsx
import { Component, Input } from '@angular/core'
import { AccountsService } from '../accounts.service'
import { LoggingService } from '../logging.service'

@Component({
	selector: 'app-account',
	templateUrl: './account.component.html',
	styleUrls: ['./account.component.css'],
})
export class AccountComponent {
	@Input() account: { name: string; status: string }
	@Input() id: number

	constructor(
		private loggingServis: LoggingService,
		private accountsService: AccountsService
	) {}

	onSetTo(status: string) {
		this.accountsService.updateAccount(this.id, status)
		this.accountsService.statusUpdated.emit(status) //emitujem event, ktory som nastavila v account.service.ts
	}
}
```

(subor new-account.component.ts)

```tsx
import { Component } from '@angular/core'
import { AccountsService } from '../accounts.service'
import { LoggingService } from '../logging.service'

@Component({
	selector: 'app-new-account',
	templateUrl: './new-account.component.html',
	styleUrls: ['./new-account.component.css'],
})
export class NewAccountComponent {
	constructor(
		private loggingServis: LoggingService,
		private accountsService: AccountsService
	) {
		// preposielanie dat z komponenty account.component.ts
		this.accountsService.statusUpdated.subscribe((status: string) =>
			alert('New Status: ' + status)
		)
	}

	onCreateAccount(accountName: string, accountStatus: string) {
		this.accountsService.addAccount(accountName, accountStatus)
	}
}
```

## Zobrazovanie a kontrola eventov

### mame dva sposoby prepojenia premennej z komponentovej classy do template htmlka

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses', // tu definujeme nazov tejto componenty a tento nazov sa pouziva ako htmlkove volanie v inej componente (pouzitie tejto componenty je <courses />)
	template: `<h2>{{ title }}</h2>
		<h2 [textContent]="title"></h2>
		<img src="{{ imageUrl }}" />
		<img [src]="imageUrl" /> `,
})
export class CoursesComponent {
	title = 'List of courses'
	imageUrl = 'http://lorempixel.com/400/200/'
}
```

### Classova vazba na premennu

- v tomto priklade mam definovanu bootstrap classu btn, ... a vramci logiky mam definovanu
  classu active len vtedy ak premenna isActive je true

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses', // tu definujeme nazov tejto componenty a tento nazov sa pouziva ako htmlkove volanie v inej componente (pouzitie tejto componenty je <courses />)
	template: `<button class="btn btn-primary" [class.active]="isActive">
		Save
	</button>`,
})
export class CoursesComponent {
	isActive = true
}
```

- v tomto priklade mam definovanu style vazbu, v ktorej viem menit background color pomocou podmienky

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses', // tu definujeme nazov tejto componenty a tento nazov sa pouziva ako htmlkove volanie v inej componente (pouzitie tejto componenty je <courses />)
	template: `<button
		class="btn btn-primary"
		[style.backgroundColor]="isActive ? 'blue' : 'white'"
	>
		Save
	</button>`,
})
export class CoursesComponent {
	isActive = true
}
```

### Event vazba

- Click vazba na htmlkovy tag sa vykona takto:

Click event musi byt vramci htmlkoveho tagu zapisany ako atribut obaleny zatvorkou a vramci `=` sa definuje hodnota, ktora je funkcia alebo metoda definovana vramci class komponenty.
Vieme vytiahnut vramci click eventu aj objekt event a tiez vieme zastavit prebublanie click eventu na rodica, ktory obsahuje tak isto click handler. Dokazeme to pomocou `$event.stopPropagation()`

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses',
	template: `<div (click)="onDivClicked()">
		<button (click)="onSave($event)" class="btn btn-primary">Save</button>
	</div>`,
})
export class CoursesComponent {
	onDivClicked() {
		console.log('Div was clicked.')
	}
	onSave($event) {
		$event.stopPropagation()
		console.log('Button was clicked.', $event)
	}
}
```

- Event filtering

Podobne ako click vieme definovat aj dalsie event posluchace. Konkretne v tomto priklade sme nad inputom handlovali `keyup`, ktory po stlaceni na tlacidlo enter nam vyvola metodu a vypise console.log. Mame dve riesenia:

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses',
	template: `<input (keyup)="onKeyUp($event)" />
		<input (keyup.enter)="onKeyUp2()" /> `,
})
export class CoursesComponent {
	onKeyUp($event) {
		if ($event.keyCode === 13) {
			console.log('Enter was pressed.')
		}
	}
	onKeyUp2() {
		console.log('Enter was pressed.')
	}
}
```

- Zachytavanie value z inputu vieme vykonat takto:

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses',
	template: `<input #email (keyup.enter)="onKeyUp(email.value)" />
		<input (keyup)="onKeyUp2($event)" />`,
})
export class CoursesComponent {
	onKeyUp(email) {
		console.log(email)
	}
	onKeyUp2($event) {
		console.log($event.target.value)
	}
}
```

### Two way binding

Obojsmerna vazba dava komponentam v aplikacii sposob zdielania udajov. Pomocou vazby obojsmerneho viazania mozete pocuvat udalosti a sucasne aktualizovat hodnoty medzi nadradenou a podradenou sucastou

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses',
	template: `<input
			[value]="email"
			(keyup.enter)="email = $event.target.value; onKeyUp()"
		/>
		<input [(ngModel)]="email" (keyup.enter)="onKeyUp()" />`,
})
export class CoursesComponent {
	email = 'my@example.com'
	onKeyUp() {
		console.log(this.email)
	}
}
```

### Pipes - [link](https://angular.io/guide/pipes)

- UpperCase, LowerCase, Decimal, Currency, Percent

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses',
	template: `
		{{ course.title | uppercase | lowercase }} <br />
		{{ course.raiting }} <br />
		{{ course.raiting | number: '1.2-2' }} <br />
		{{ course.raiting | number: '2.2-2' }} <br />
		{{ course.raiting | number: '1.1-1' }} <br />
		{{ course.raiting | number: '2.1-1' }} <br />
		{{ course.students | number }} <br />
		{{ course.price | currency }} <br />
		{{ course.price | currency: 'EUR':true:'3.2-2' }} <br />
		{{ course.releaseDate }} <br />
		{{ course.releaseDate | date: 'shortDate' }} <br />
	`,
})
export class CoursesComponent {
	course = {
		title: 'nejaky text',
		raiting: 4.9745,
		students: 30123,
		price: 190.956,
		releaseDate: new Date(2016, 3, 1),
	}
}
```

- vlastna pipe

```tsx
import { Pipe, PipeTransform } from '@angular/core'
@Pipe({ name: 'summary' })
export class SummaryPipe implements PipeTransform {
	transform(
		value: string,
		limit?: number,
		firstSymbol?: string,
		endSymbol?: string
	) {
		if (!value) {
			return null
		}
		let actualLimit = limit ? limit : 50
		let first = firstSymbol ? firstSymbol : ''
		let end = endSymbol ? endSymbol : ''
		return first + value.substr(0, actualLimit) + '...' + end
	}
}
```

Po definovani vlastnej pipe musim zadefinovat v app.module.ts declarations v tejto vlastnej pipy,
ktora je vyexportovana zo suboru. Nasledne tato pipe je dostupna globalne v kazdej komponente.
Pouzitie tejto vlastnej komponenty:

```tsx
import { Component } from '@angular/core'
@Component({
	selector: 'courses',
	template: ` {{ text | summary: 10:'aaa ':' bbb' }}`,
})
export class CoursesComponent {
	text = 'Jelenovi pivo nelej, ked nema co pit. Ked ma co pit, tak mu nalej.'
}
```

### Budovanie znovupouzitelnych komponent

- Component API

Vramci komponenty v subore html mozme pouzivat htmlkove tagy a uz definovane komponenty s htmlkovymi tagmy. Pri htmlkovych tagoch vieme definovat atribut a k nemu prisluchajucu premennu a pri definovanych komponentach vieme zasielat cez atribut nami volene premenne.

```html
<img [src]="imageUrl" />
<button (click)="onClick()">Click me!</button>
<favorite [isFavorite]="post.isFavorite"></favorite>
```

- Vstupna hodnota preposlana do componenty zvonku pomocou dekoratora `@Input()`:

```tsx
import { Component, Input } from '@angular/core' // Importnuty dekorator Input

@Component({
	selector: 'favorite',
	templateUrl: './favorite.component.html',
	styleUrls: ['./favorite.component.scss'],
})
export class FavoriteComponent {
	@Input('isFavorite') isSelected: boolean // Definovali sme dekorator Input pre vstupnu premennu zvonku, tento dekorator nam umoznuje pouzivat alias, to znamena pomenovat si tuto vstupnu premennu s inym nazvom, napr. isSelected

	onClick() {
		this.isSelected = !this.isSelected
	}
}
```

Tym, ze sme pouzili dekorator @Input() a definovali si iny nazov pre vstupnu premennu isFavorite na isSelected, nesmieme zabudnut pouzit novy nazov aj v htmlku.

```html
<i
	class="glyphicon icon"
	[class.glyphicon-star]="isSelected"
	[class.glyphicon-star-empty]="!isSelected"
	(click)="onClick()"
></i>
```

Definovanie vstupnej premennej pre componentu, konkretne premennej isFavorite z app.component.ts

```html
<favorite [isFavorite]="post.isFavorite"></favorite>
```

```tsx
import { Component } from '@angular/core'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	post = { isFavorite: true } // definovanie premennej post, ktora sa pouzije v htmlku
}
```

- Vstupna hodnota preposlana do componenty zvonku pomocou dekoratora `@Component()`:
  Neumoznuje nam pouzit alias

```tsx
import { Component } from '@angular/core'

@Component({
	selector: 'favorite',
	templateUrl: './favorite.component.html',
	styleUrls: ['./favorite.component.scss'],
	inputs: ['isFavorite'], // Definovanie preposlanej vstupnej hodnoty zvonku
})
export class FavoriteComponent {
	isFavorite: boolean

	onClick() {
		this.isFavorite = !this.isFavorite
	}
}
```

- Dekorator @Output()

Definovanie atributu change posluchaca s metodou, ktora je definovana vramci app.component.ts

```html
<favorite
	[isFavorite]="post.isFavorite"
	(change)="onFavoriteChanged()"
></favorite>
```

Vramci tejto komponenty mame definovanu metodu onFavoriteChanged() a tato metoda je volana z vnutorneho scopu komponenty favorite

```tsx
import { Component } from '@angular/core'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	post = { isFavorite: true }
	onFavoriteChanged() {
		console.log('favorite changed')
	}
}
```

```tsx
import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
	selector: 'favorite',
	templateUrl: './favorite.component.html',
	styleUrls: ['./favorite.component.scss'],
})
export class FavoriteComponent {
	@Input('isFavorite') isSelected: boolean
	@Output() change = new EventEmitter() // Definovanie dekoratora @Output() pre premennu change. Tato premenna obsahuje instanciu classy EventEmitter(). EventEmitter() sa pouziva na vytvorenie vazby funkcie s udalostou, emit sa pouziva na spustenie udalosti.

	onClick() {
		this.isSelected = !this.isSelected
		this.change.emit()
	}
}
```

- Predavanie udajov udalosti

```tsx
import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
	selector: 'favorite',
	templateUrl: './favorite.component.html',
	styleUrls: ['./favorite.component.scss'],
})
export class FavoriteComponent {
	@Input('isFavorite') isSelected: boolean
	@Output() change = new EventEmitter()

	onClick() {
		this.isSelected = !this.isSelected
		this.change.emit(this.isSelected) // preposlanie premennej isSelected
	}
}
```

Vramci premennej change metoda onFavoriteChanged() ocakava vtupnu premennu zaslanu z vnutra komponenty favorite

```html
<favorite
	[isFavorite]="post.isFavorite"
	(change)="onFavoriteChanged($event)"
></favorite>
```

```tsx
import { Component } from '@angular/core'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	post = { isFavorite: true }
	onFavoriteChanged(eventArgs) {
		// Nacitana vtupna premenna, ktora je zaslana zvonku
		console.log('favorite changed: ', eventArgs)
	}
}
```

- Definovanie typu vystupnej premennej

```tsx
import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
	selector: 'favorite',
	templateUrl: './favorite.component.html',
	styleUrls: ['./favorite.component.scss'],
})
export class FavoriteComponent {
	@Input('isFavorite') isSelected: boolean
	@Output() change = new EventEmitter()

	onClick() {
		this.isSelected = !this.isSelected
		this.change.emit({ newValue: this.isSelected }) // Preposielanie premennej objekt s hodnotou newValue
	}
}
```

```tsx
import { Component } from '@angular/core'
// Definovanie, ake premenne obsahuje {}, ktory vchadza do metody
interface favoriteChangedEventArgs {
	newValue: boolean
}

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	post = { isFavorite: true }
	onFavoriteChanged(eventArgs: favoriteChangedEventArgs) {
		// Definovanie typu vystupnej premennej
		console.log('favorite changed: ', eventArgs.newValue)
	}
}
```

- Pridavanie aliasu pre @Output() dekorator

```tsx
import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
	selector: 'favorite',
	templateUrl: './favorite.component.html',
	styleUrls: ['./favorite.component.scss'],
})
export class FavoriteComponent {
	@Input('isFavorite') isSelected: boolean
	@Output('change') click = new EventEmitter() // Ak pridam alias, musim definovat v Output metode preposlanu metodu, napr. change

	onClick() {
		this.isSelected = !this.isSelected
		this.click.emit({ newValue: this.isSelected })
	}
}
```

- Template

```tsx
import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
	selector: 'favorite',
	// templateUrl: './favorite.component.html',
	template: `<i
		class="glyphicon icon"
		[class.glyphicon-star]="isSelected"
		[class.glyphicon-star-empty]="!isSelected"
		(click)="onClick()"
	></i>`,
	styleUrls: ['./favorite.component.scss'],
})
export class FavoriteComponent {
	@Input('isFavorite') isSelected: boolean
	@Output('change') click = new EventEmitter()

	onClick() {
		this.isSelected = !this.isSelected
		this.click.emit({ newValue: this.isSelected })
	}
}
```

- Styles

Mozme stylovat pomocou premennej styleUrls, ktora ocakava pole stringov, v ktorych su definovane cesty k css suborom. Alebo pomocou styles premennej, ktora ocakava pole stringov, v ktorych mam definovanie css. Alebo definujem css priamo v html pomocou <style></style>.

```tsx
import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
	selector: 'favorite',
	templateUrl: './favorite.component.html',
	// template: `<i
	//   class="glyphicon icon"
	//   [class.glyphicon-star]="isSelected"
	//   [class.glyphicon-star-empty]="!isSelected"
	//   (click)="onClick()"
	// ></i>`,
	styleUrls: ['./favorite.component.scss'],
	styles: [
		`
			.glyphicon {
				font-size: 50px;
			}
		`,
	],
})
export class FavoriteComponent {
	@Input('isFavorite') isSelected: boolean
	@Output('change') click = new EventEmitter()

	onClick() {
		this.isSelected = !this.isSelected
		this.click.emit({ newValue: this.isSelected })
	}
}
```

- ngContent

Definovanie komponenty panel:
pomocou ng-content s atributom select viem nadefinovat prepojenie zaslaneho obsahu.

```html
<div class="panel panel-default">
	<div class="panel-heading"><ng-content select=".heading"></ng-content></div>
	<div class="panel-body"><ng-content select=".body"></ng-content></div>
</div>
```

Pouzitie uz vytvorenej komponenty panel a zasielanie obsahu do tejto komponenty:

```html
<panel>
	<div class="heading">Heading</div>
	<div class="body">
		<h2>Body</h2>
		<p>blablbakkdkfkfkgkgkgk</p>
	</div>
</panel>
```

- ngContainer

Je to podobna schopnost ako v Reacte funguje fragment. Dokaze preposielat viacero elementov alebo obsah, bez toho aby sa do DOMu vkladalo navyse jedno divko

```html
<panel>
	<ng-container class="heading">Heading</ng-container>
	<ng-container class="body">
		<h2>Body</h2>
		<p>blablbakkdkfkfkgkgkgk</p>
	</ng-container>
</panel>
```

- View encapsulation je schopnost, ktorou vieme definovat pouzitie stylov pre danu komponentu, aby sa neprepisovalo css pre ine komponenty. Shadow DOM API je klucovou sucastou, poskytuje sposob ako pripojit skryty oddeleny DOM k prvku. Da sa pouzit len pre moderne prehliadace. Nastavenie pre encapsulation moze byt taketo:

```tsx
import { Component, ViewEncapsulation } from '@angular/core'

// enum ViewEncapsulation {
//   Emulated = 0, - pomocou tohto nastavenia mozme definovat styly nie v shadow DOMe a vsetky selektory v css maju pridany angularovsky hash - atribut, ktory umoznuje definovanie css len pre danu komponentu. Defaltna moznost.
//   None = 2, - nemame ziadne definovanie stylov v shadow DOMe a nemame ziadne prepisovanie selektorov v css na angularovsky atribut, cize nase css sa stane globalnym
//   ShadowDom = 3, - pomocou tohto nastavenia mozeme definovat to, ze sa pridaju vsetky styly do shadow DOMu, s tym, ze css pre danu komponentu s tymto nastavenim nema zaheshovanie, ale ostatne maju. Vsetko css je pridane do shadow root.
// }

@Component({
	selector: 'encapsulation-example',
	template: `
		<h1>Hello World!</h1>
		<span class="red">Shadow DOM Rocks!</span>
	`,
	styles: [
		`
			:host {
				display: block;
				border: 1px solid black;
			}
			h1 {
				color: blue;
			}
			.red {
				background-color: red;
			}
		`,
	],

	encapsulation: ViewEncapsulation.Emulated,
})
export class EncapsulationExampleComponent {}
```

[link](https://angular.io/api/core/ViewEncapsulation)

### Direktivy

- ngIf
- ngFor
- ngSwitchCase
- ngClass
- ngStyle
- vlastna direktiva

#### ngIf

- Tato direktiva sa pouziva na zobrazenie elementov, ak je podmienka splnena (je true), tak sa zobrazi dany element

```html
<div *ngIf="courses.length > 0">List of courses</div>
<div *ngIf="courses.length === 0">No courses yet</div>
```

- Ak je podmienka nepravdiva a chcem definovat `else` vystup, mozem to urobit pomocou `ng-template` a zlinkovania napriklad noCourses

```html
<div *ngIf="courses.length > 0; else noCourses">List of courses</div>
<ng-template #noCourses>No courses yet</ng-template>
```

- if else logika zobrazenia dalsim sposobom

```html
<div *ngIf="courses.length > 0; then coursesList; else noCourses"></div>
<ng-template #coursesList>List of courses</ng-template>
<ng-template #noCourses>No courses yet</ng-template>
```

```html
<div *ngIf="courses.length > 0">List of courses</div>
<ng-template [ngIf]="courses.length > 0"
	><div>List of courses</div></ng-template
>
<ng-template [ngIf]="!(courses.length > 0)">No courses yet</ng-template>
```

- okrem *ngIf mozme pouzit na zobrazenie aj `hidden atribut`. Tento atribut pridava styl `display: none` len ak je podmienka splnena (ked je true). Cize opak if logiky. Pre male komponenty sa moze pouzit atribut hidden a pre velke komponenty a zanorenie sa pouziva *ngIf.

```html
<div [hidden]="courses.length > 0">List of courses</div>
<div [hidden]="courses.length === 0">No courses yet</div>
```

#### direktiva ngSwitchCase

```tsx
import { Component } from '@angular/core'

@Component({
	selector: 'switch-case-example',
	templateUrl: './switch-case-example.component.html',
	styleUrls: ['./switch-case-example.component.scss'],
})
export class SwitchCaseExampleComponent {
	viewMode: string = 'map'
}
```

```html
<ul class="nav nav-pills">
	<li [class.active]="viewMode == 'map'">
		<a (click)="viewMode = 'map'">Map view</a>
	</li>
	<li [class.active]="viewMode == 'list'">
		<a (click)="viewMode = 'list'">List view</a>
	</li>
</ul>
<div [ngSwitch]="viewMode">
	<div *ngSwitchCase="'map'">Map view content</div>
	<div *ngSwitchCase="'list'">List view content</div>
	<div *ngSwitchDefault>Otherwise</div>
</div>
```

#### direktiva ngFor

- sluzi na vykonanie nejakeho cyklu.

```html
<ul>
	<li *ngFor="let course of courses; index as i">
		{{ i }} - {{ course.name }}
	</li>
</ul>
<ul>
	<li *ngFor="let course of courses; even as i">{{ i }} - {{ course.name }}</li>
</ul>
<ul>
	<li *ngFor="let course of courses; even as isEven">
		{{ course.name }}<span *ngIf="isEven">Even</span>
	</li>
</ul>
```

```tsx
import { Component } from '@angular/core'

interface coursesItemType {
	id: number
	name: string
}

@Component({
	selector: 'ng-for-example',
	templateUrl: './ng-for-example.component.html',
	styleUrls: ['./ng-for-example.component.scss'],
})
export class NgForExampleComponent {
	courses: coursesItemType[] = [
		{ id: 1, name: 'course1' },
		{ id: 2, name: 'course2' },
		{ id: 3, name: 'course3' },
	]
}
```

- `trackBy` je funkcia, ktora definuje ako sledovat zmeny pre prvky v poli vramci ngFor cyklu. Ak vymazem alebo zmenim stav vramci jedneho prvku z pola touto optimalizaciou sa neprekresluju vsetky prvky z pola, to jest vsetky li elementu ale namiesto toho sa prekresli len ten jeden dotycny prvok, teda jedno li.

```html
<button (click)="loadCourses()">Load courses list</button>
<ul>
	<li *ngFor="let course of courses; trackBy: trackCourse">
		{{ course.name }}
	</li>
</ul>
```

```tsx
import { Component } from '@angular/core'

interface coursesItemType {
	id: number
	name: string
}

@Component({
	selector: 'for-example-track-by',
	templateUrl: './for-example-track-by.component.html',
	styleUrls: ['./for-example-track-by.component.scss'],
})
export class ForExampleTrackByComponent {
	courses: coursesItemType[]

	loadCourses() {
		this.courses = [
			{ id: 0, name: 'course0' },
			{ id: 1, name: 'course1' },
			{ id: 2, name: 'course2' },
		]
	}

	trackCourse(index, course) {
		return course ? course.id : undefined
	}
}
```

[link](https://angular.io/api/common/NgForOf)

#### ngClass

Sposob zapisovania viacerych class vramci jednej premennej pomocou `ngClass`

```html
<i
	class="glyphicon icon"
	[class.glyphicon-star]="isSelected"
	[class.glyphicon-star-empty]="!isSelected"
	(click)="onClick()"
></i>
```

Pridavanie class pomocou `ngClass`

```html
<i
	class="glyphicon icon"
	[ngClass]="{
    'glyphicon-star': isSelected,
    'glyphicon-star-empty': !isSelected
  }"
	(click)="onClick()"
></i>
```

#### ngStyle

Pridavanie stylov pre elementy

```html
<button
	(click)="handleClick()"
	[style.backgroundColor]="canSave ? 'blue' : 'grey'"
	[style.color]="canSave ? 'white' : 'black'"
	[style.fontWeight]="canSave ? 'bold' : 'normal'"
>
	Save
</button>
```

Pridavanie stylov pomocou `ngStyle`

```html
<button
	(click)="handleClick()"
	[ngStyle]="{
    backgroundColor: canSave ? 'blue' : 'grey',
    color: canSave ? 'white' : 'black',
    fontWeight: canSave ? 'bold' : 'normal'
  }"
>
	Save
</button>
```

#### Save traversal operator

Vramci nacitania premennych vieme kontrolovat ci dana premenna existuje. Ak neexistuje vieme tento stav odchytit a nezobrazi sa nam error v konzole.

```tsx
import { Component } from '@angular/core'

@Component({
	selector: 'custom-directiva-example',
	templateUrl: './custom-directiva-example.component.html',
	styleUrls: ['./custom-directiva-example.component.scss'],
})
export class CustomDirectivaExampleComponent {
	task = { title: 'review applications', assignee: null }
}
```

```html
<p *ngIf="task.assignee">{{ task.assignee.name }}</p>
<p>{{ task.assignee?.name }}</p>
```

#### Custom direktiva

```html
<input type="text" [format]="'uppercase'" appInputFormat />
```

```tsx
import { Directive, HostListener, ElementRef, Input } from '@angular/core'

@Directive({
	selector: '[appInputFormat]',
})
export class CustomDirectivaExampleComponent {
	@Input('format') format

	constructor(private el: ElementRef) {}
	// @HostListener('focus') onFocus() {
	//   console.log('onFocus');
	// }

	@HostListener('blur') onBlur() {
		let value: string = this.el.nativeElement.value
		if (this.format === 'lowercase') {
			this.el.nativeElement.value = value.toLowerCase()
		} else {
			this.el.nativeElement.value = value.toUpperCase()
		}
	}
}
```

```html
<input type="text" [appInputFormat]="'uppercase'" />
```

```tsx
import { Directive, HostListener, ElementRef, Input } from '@angular/core'

@Directive({
	selector: '[appInputFormat]',
})
export class CustomDirectivaExampleComponent {
	@Input('appInputFormat') format
	constructor(private el: ElementRef) {}
	// @HostListener('focus') onFocus() {
	//   console.log('onFocus');
	// }

	@HostListener('blur') onBlur() {
		let value: string = this.el.nativeElement.value
		if (this.format === 'lowercase') {
			this.el.nativeElement.value = value.toLowerCase()
		} else {
			this.el.nativeElement.value = value.toUpperCase()
		}
	}
}
```

### Forms - Sablonove formulare (template driven) od Angularu

Ak chceme mat pristup k vsetkym schopnostiam `angularovskych formularov`, musime importnut `FormsModule` a definovat to v imports.

```tsx
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'

@NgModule({
	declarations: [AppComponent],
	imports: [BrowserModule, AppRoutingModule, FormsModule],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
```

#### Forms - Sablonove formulare (template driven) od Angularu - ngModel

Angular nevie automaticky detekovat nase inputy vo forme. Nase kontrolory v inputoch musime zadat manualne, aby angular vedel ktore inputy si ma vsimnut pri submite. Na to vyuzivame `ngModel`. Na to aby ngModel fungoval musime dat angularu este jednu informaciu a to je `name="..."` daneho inputu.

Pomocou direktivy ngModel vieme z formularovych elementov ziskat hodnotu atributu, teda ich value. ngModel ma vzdy aktualny stav elementu. Tento stav je prepisany pri kazdej akcii vramci elementu.

ngModel vieme pri formularoch vyuzit 3 sposobmi:

- bez vazby (no binding) `ngModel`, kde len hovorime angularu, ze dany input je control vo forme
- jednosmerna vazba (one way binding) `[ngModel]="nazovPremennej"`, pomocou ktorej vieme nastavit default hodnotu controloru
- obojsmerna vazva (two way binding) `[(ngModel)]="nazovPremennej"`, pomocou ktorej vieme dynamicky zasielat, vykreslovat zadanu hodnotu

```html
<form>
	<div id="user-data">
		<div class="form-group">
			<label for="username">Username</label>
			<input
				type="text"
				id="username"
				class="form-control"
				ngModel
				name="username"
			/>
		</div>
		<button class="btn btn-default" type="button">Suggest an Username</button>
		<div class="form-group">
			<label for="email">Mail</label>
			<input
				type="email"
				id="email"
				class="form-control"
				ngModel
				name="email"
			/>
		</div>
	</div>
	<div class="form-group">
		<label for="secret">Secret Questions</label>
		<select id="secret" class="form-control" ngModel name="secret">
			<option value="pet">Your first Pet?</option>
			<option value="teacher">Your first teacher?</option>
		</select>
	</div>
	<button class="btn btn-primary" type="submit">Submit</button>
</form>
```

```html
<form>
	<div class="form-group">
		<label for="name"><strong>Server Name</strong></label>
		<input
			id="name"
			type="text"
			class="form-control"
			placeholder="Server Name"
			[(ngModel)]="newServerName"
		/>
	</div>
	<button class="btn btn-primary" (click)="addGreenServers()">
		Add Green Server
	</button>
</form>
```

```tsx
import { Component } from '@angular/core'

@Component({
	selector: 'cockpit',
	templateUrl: './cockpit.component.html',
	styleUrls: ['./cockpit.component.scss'],
})
export class CockpitComponent {
	serverName = ''

	addGreenServers() {
		console.log(this.serverName)
	}
}
```

`ngModel` mozme vyuzit aj pri `select` inpute, kedy si rozkliknutim menu vyberame z ponuknutych moznosti. Na to, aby sa nam zobrazila niektora z moznosti ako defaultna (aby select input neostal na zaciatku prazdny) mozme vyuzit ngModel:

(zo suboru app.component.html)

```html
<label for="secret">Secret Questions</label>
<select
	id="secret"
	class="form-control"
	[ngModel]="defaultQuestion"
	name="secret"
>
	<option value="pet">Your first Pet?</option>
	<option value="teacher">Your first teacher?</option>
</select>
```

Vytvorime si premennu `defaultQuestion`, ktora sa bude rovnat jednej z hodnot options z nasho selectu.

(subor app.component.ts)

```tsx
import { Component, ViewChild } from '@angular/core'
import { NgForm } from '@angular/forms'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
})
export class AppComponent {
	@ViewChild('f') signupForm: NgForm
	defaultQuestion = 'teacher'

	suggestUserName() {
		const suggestedName = 'Superuser'
	}

	onSubmit() {
		console.log(this.signupForm)
	}
}
```

#### Forms - Sablonove formulare (template driven) od Angularu - ngModelGroup

`ngGroupModel` vyuzivame, ak chceme spojit niektore hodnoty do jedneho celku, ak chceme vytvorit strukturu nasho objektu (napriklad v pripade, ze nas form je velmi velky). Tak isto nam to pomoze v pripade, ze budeme chciet zvalidovat skupinu inputov.

V tomto pripade mame div element, v ktorom sa nachadzaju inputy. Divku sme pridali direktivu `ngModelGroup`. ngModelGroup sa musi rovnat stringu. Napriklad sa bude rovnat stringu userData. userData bude `klucovym menom` pre tuto skupinu. Ak by sme sa pozreli do prehliadaca cez dev tools a rozklikli si `value`, mali by sme tam novy objekt s nazvom `userData`, ktory by obsahoval dalsi objekt s emailom a usernamom a ich hodnotami (pretoze v nasej skupine mame input email a input user name). Tak isto ak by sme si pozreli `controls`, nasli by sme tam `userData` control so vsetkymi premennymi, ktore pri controls pozname, ako napriklad `valid`.

Pridanim lokalnej referencie `#userData="ngModelGroup"` ziskame pristup ku javascript objektu, co nam napriklad umozni zobrazit spravu, ze user data nie su validne v pripade, ze user data boli dotknute a neboli validne.

(zo suboru app.component.html)

```html
<div id="user-data" ngModelGroup="userData" #userData="ngModelGroup">
	<div class="form-group">
		<label for="username">Username</label>
		<input
			type="text"
			id="username"
			class="form-control"
			ngModel
			name="username"
			required
		/>
	</div>
	<button class="btn btn-default" type="button">Suggest an Username</button>
	<div class="form-group">
		<label for="email">Mail</label>
		<input
			type="email"
			id="email"
			class="form-control"
			ngModel
			name="email"
			required
			email
			#email="ngModel"
		/>
		<span class="help-block" *ngIf="!email.valid && email.touched"
			>Please enter a valid email!</span
		>
	</div>
</div>
<p *ngIf="!userData.valid && userData.touched">User data is invalid!</p>
```

#### Forms - Sablonove formulare (template driven) od Angularu - submitovanie a pouzivanie formularu, # mriezkova referencia

Na submitovanie formulara budeme vyuzivat metodu `onSubmit()`. Aby sme ju mohli pouzit, musime si ju zadefinovat, napr:

(zo suboru app.component.ts)

```tsx
onSubmit(form: NgForm) {
    console.log(form);
  }
```

..a potom ju musime niekde v html subore zavolat. Mohlo by sa zdat, ze vhodne miesto na jej umiestnenie by bol button:

(zo suboru app.component.html)

```html
<button class="btn btn-primary" type="submit">Submit</button>
```

..ale nie je to tak. Tento button je typu `submit`, takze ak nanho klikneme nastane defaultne spravanie htmlka --> ak mame button vo form elemente, tento button submitne form, zasle request, ale okrem toho este spusti javascriptovy event (`submit event`). Angular toto preddefinovane spravanie vyuziva a dava nam directivu, ktoru mozme umiestnit do form elementu. Direktiva sa vola `ngSubmit` a dava nam len jeden event, ktory mozme vyuzit. Tento event sa spusti vzdy ked je form submitnuty. Toto je vhodne miesto na volanie `onSubmit()` metody.

(zo suboru app.component.html)

```html
<form (ngSubmit)="onSubmit()"></form>
```

Ak chceme mat dostupne aktualne hodnoty z formularu, vyuzijeme `#nazovReferencie` (mriezkovu referenciu) a vlozime ju do `onSubmit(f)` metody. Na to, aby sme sa skutocne dostali ku vsetkym hodnotam, ku javascriptovemu objektu vytvorenemu angularom, sa ale musi nasa referencia `#f` niecomu rovnat, a to `ngForm`. Tymto zapisom hovorime angularu, aby nam dal pristup ku formularu, ktory vytvoril automaticky.

(zo suboru app.component.html)

```html
<form (ngSubmit)="onSubmit(f)" #f="ngForm"></form>
```

(zo suboru app.component.html)

```tsx
import { NgForm } from "@angular/forms";

 onSubmit(form: NgForm) {
    console.log(form);
  }
```

#### Pouzitie # ako referencie v html template

Vramci htmlka si definujem referencnu premennu pomocou `#mojVlastnyNazovPremennej`. Tuto premennu viem vramci htmlka kdekolvek pouzit. V tomto priklade som tuto premennu poslala do metody addGreenServers() ako vstupny parameter. Referencna premenna vzdy obsahuje len selektnuty element a jeho atributy.

```html
<form>
	<div class="form-group">
		<label for="name"><strong>Server Name</strong></label>
		<input
			id="name"
			type="text"
			class="form-control"
			placeholder="Server Name"
			#newServerName
		/>
	</div>
	<button class="btn btn-primary" (click)="addGreenServers(newServerName)">
		Add Green Server
	</button>
</form>
```

```tsx
import { Component } from '@angular/core'

@Component({
	selector: 'cockpit',
	templateUrl: './cockpit.component.html',
	styleUrls: ['./cockpit.component.scss'],
})
export class CockpitComponent {
	addGreenServers(newServerName: HTMLInputElement) {
		console.log(newServerName.value)
	}
}
```

#### Forms - Sablonove formulare od Angularu - submitovanie a pouzivanie formularu, @ViewChild

Dalsi sposob, ako sa dopracovat ku hodnotam z formularu je vyuzit `@ViewChild` namiesto `#`, ktoru sme zasielali do `onSubmit()` metody vo form elemente.

(subor app.component.ts)

```html
<form (ngSubmit)="onSubmit()" #f="ngForm"></form>
```

(subor app.component.ts)

```tsx
import { Component, ViewChild } from '@angular/core'
import { NgForm } from '@angular/forms'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
})
export class AppComponent {
	@ViewChild('f') signupForm: NgForm

	onSubmit() {
		console.log(this.signupForm)
	}
}
```

#### @ViewChild

Je to dekorator, ktory selektne vyhladavany element v zobrazenom DOMe a zachyti jeho zmeny. Tento dekorator nam vracia obsah elementu vramci DOMu, t.j. vsetky schopnosti elementu vramci DOMu. Tento dekorator mozme pouzivat od angularovskej verzie 8+. ViewChild sa da napojit na elementy len vramci svojej komponenty.

```html
<form>
	<div class="form-group">
		<label for="name"><strong>Server Name</strong></label>
		<input
			id="name"
			type="text"
			class="form-control"
			placeholder="Server Name"
			#newServerName
		/>
	</div>
	<button class="btn btn-primary" (click)="addGreenServers()">
		Add Green Server
	</button>
</form>
```

```tsx
import { Component } from '@angular/core'

@Component({
	selector: 'cockpit',
	templateUrl: './cockpit.component.html',
	styleUrls: ['./cockpit.component.scss'],
})
export class CockpitComponent {
	@ViewChild('newServerName') newServerName: ElementRef

	addGreenServers() {
		console.log(this.newServerName.nativeElement.value)
	}
}
```

#### Forms - Sablonove formulare (template driven) od Angularu - validacia inputov

Ak chceme zvalidovat inputy, napriklad chceme pozadovat, aby jednotlive inputy museli byt vyplnene (aby nemohli ostat bez nejakej hodnoty) vyuzijeme validator `required`. Je to defaultny html atribut, ktory mozeme pridat do inputu. V nasom pripade ho angular vie detekovat a pouzit ho ako selektor, bude vediet automaticky naprogramovat nas formular tak, ze input bude povazovat za nevalidny, ak v nom nebude ziadna hodnota.

```tsx
<label for="username">Username</label>
<input
	type="text"
	id="username"
	class="form-control"
	ngModel
	name="username"
	required
/>
```

Tak isto existuje aj validacia pre e-mail input, direktiva `email`. Nie je to ale defaultny html atribut, email je direktiva vytvorena angularom.

```tsx
<label for="email">Mail</label>
<input
	type="email"
	id="email"
	class="form-control"
	ngModel
	name="email"
	required
	email
/>
```

Angular dynamicky pridava niektore css classy, tym nam dava informaciu o stavoch jednotlivych inputov.
Napriklad ci je input `dirty` - teda ci sa zmenila pociatocna hodnota.
Ci bol input `touched` - teda ci sa donho kliklo alebo este nekliklo.
A ci je `valid` - ci je validny alebo nie je.

Angular Validatori pre `reactive forms`: [link](https://angular.io/api/forms/Validators)

Angular Validatori pre `template-driven forms`: [link](https://angular.io/api?type=directive)

- EmailValidator
- MaxLengthValidator
- PatternValidator
- RequiredValidator
- CheckboxRequiredValidator
- MinLengthValidator

#### Porovnanie ngModel, #mriezkova referencia, @ViewChild

- ngModel vramci handlovania nam vzdy vrati len aktualny stav danej premennej, t.j. value z inputu.
- #mriezkova referencia je premenna, ktora nam vrati htmlkovy element plus jeho atributy. Zo selektnuteho elementu mozme vytiahnut aktualny stav value cez bodku (element.value).
- @ViewChild dekorator selektuje nami definovany element vramci DOMu. Dekorator nam vytahuje vsetok obsah selektnuteho elementu, t.j. css, metody, atributy, vsetko co moze byt vramci DOMu definovane pre dany selektnuty element (this.element.nativeElement.value).

#### Forms - Sablonove formulare (template driven) od Angularu - radio buttons

Vytvorime si premennu `gender`. Bude to pole, ktore cez `*ngFor` pouzijeme v html subore.

(subor app.component.ts)

```tsx
import { Component, ViewChild } from '@angular/core'
import { NgForm } from '@angular/forms'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
})
export class AppComponent {
	@ViewChild('f') signupForm: NgForm
	defaultQuestion = 'teacher'
	answer = ''
	genders = ['male', 'female']

	suggestUserName() {
		const suggestedName = 'Superuser'
	}

	onSubmit() {
		console.log(this.signupForm)
	}
}
```

Input bude typu `radio`. `Name` bude gender, pretoze pri radio buttonoch si vzdy mozeme vybrat len jednu z moznosti. Pridame aj `ngModel`, aby sme angularu dali vediet, ze je to control vo formulari. `[value]` sa bude rovnat gender. Vieme nastavit aj defaultnu hodnotu pre radio button, a to tak, ze na ngModel dame jednosmernu vazbu, napr `[ngModel]="genderType"` a v app.component.ts by sme si vytvorili premennu `genderType = "male"`.

(zo suboru app.component.html)

```html
<div class="radio" *ngFor="let gender of genders" required>
	<label>
		<input type="radio" name="gender" ngModel [value]="gender" />
		{{ gender }}
	</label>
</div>
```

#### Forms - Sablonove formulare (template driven) od Angularu - setValue(), patchValue()

- `setValue()` sluzi na nastavenie (prepisanie) celeho nasho formu
- `patchValue()` je dostupne iba pre form, ktory obsahuje `ngForm`. Sluzi na prepisanie vybranych casti formu

```html
<form (ngSubmit)="onSubmit(f)" #f="ngForm">
	<div id="user-data" ngModelGroup="userData" #userData="ngModelGroup">
		<div class="form-group">
			<label for="username">Username</label>
			<input
				type="text"
				id="username"
				class="form-control"
				ngModel
				name="username"
				required
			/>
		</div>
		<button class="btn btn-default" type="button" (click)="suggestUserName()">
			Suggest an Username
		</button>
		<div class="form-group">
			<label for="email">Mail</label>
			<input
				type="email"
				id="email"
				class="form-control"
				ngModel
				name="email"
				required
				email
				#email="ngModel"
			/>
			<span class="help-block" *ngIf="!email.valid && email.touched"
				>Please enter a valid email!</span
			>
		</div>
	</div>
</form>
```

(subor app.component.ts)

```tsx
import { Component, ViewChild } from'@angular/core'
import { NgForm } from '@angular/forms'

@Component({
	selector:'app-root',
	templateUrl: './app.component.html',
	styleUrls:['./app.component.css'], })

export class AppComponent {
@ViewChild('f') signupForm: NgForm defaultQuestion = 'teacher'
answer = ''
genders = ['male','female']

suggestUserName() {
	const suggestedName = 'Superuser'
	this.signupForm.setValue({ userData: {
		username: suggestedName, email: '',
	},
	secret: 'pet',
	questionAnswer: '',
	gender: 'male',
		})
	}

onSubmit() {
console.log(this.signupForm)
	}
}
```

(subor app.component.ts)

```tsx
import {Component, ViewChild } from '@angular/core'
import { NgForm } from'@angular/forms'

@Component({
	selector: 'app-root',
	templateUrl:
'./app.component.html',
styleUrls: ['./app.component.css'], })

export class AppComponent {

@ViewChild('f') signupForm: NgForm defaultQuestion = 'teacher'
answer = ''
genders = ['male', 'female']

suggestUserName() {
	const suggestedName = 'Superuser'
	this.signupForm.form.patchValue({ userData: {
		username: suggestedName, }, }) }

	onSubmit() {
		console.log(this.signupForm)
	}
}
```

#### Data sharing medzi komponentami

Parent to Child: via Input()
Child to Parent: via Output() and EventEmitter()
Child to Parent: via ViewChild()
Unrelated Components: via a Service

##### Priklad:

input a textarea Input ma pomocou atributu `required` schopnost
validovat value, ktora nesmie byt prazdna. `ngModel` vytvara instanciu form
control a naviaze formular na formularove schopnosti.

```html
<form>
	<div class="form-group">
		<label for="firstName">First Name</label>
		<input
			ngModel
			name="firstName"
			#firstName="ngModel"
			id="firstName"
			type="text"
			class="form-control"
			required
		/>
		<div
			*ngIf="firstName.touched && !firstName.valid"
			class="alert alert-danger"
		>
			First name is required!
		</div>
	</div>
	<div class="form-group">
		<label for="comment">Comment</label>
		<textarea
			ngModel
			name="comment"
			cols="30"
			rows="10"
			id="comment"
			type="text"
			class="form-control"
		></textarea>
	</div>
	<button class="btn btn-primary">Submit</button>
</form>
```

#### @ContentChild()

Je to dekorator, ktory selektne vyhladavany element v
zobrazenom DOMe a zachyti jeho zmeny. Tento dekorator nam vracia obsah elementu
vramci DOMu, t.j. vsetky schopnosti elementu vramci DOMu. Tento dekorator mozme
pouzivat od angularovskej verzie 8+. ContentChild sa da napojit na elementy,
ktore su preposlane do rodicovskej komponenty cez ng-content.
App komponenta ma taketo html:

./src/app/app.component.html

```html
<child-element>
	<div #test>Test</div>
</child-element>
```

Child komponenta ma taketo html:

./src/child-element/child-element.component.html

```html
<ng-content></ng-content>
```

./src/child-element/child-element.component.ts

```tsx
import { Component,
ContentChild, OnInit } from '@angular/core' @Component({ selector:
'child-element', templateUrl: './child-element.component.html', styleUrls:
['./child-element.component.scss'], }) export class ChildElementComponent
implements OnInit { @ContentChild('test') selektorDiv ngOnInit() {
console.log('ngOnInit', this.selektorDiv) } ngAfterContentInit() {
console.log('ngAfterContentInit', this.selektorDiv) } }
```

#### @ViewChild() vs. @ContentChild()

- @ViewChild() sa da napojit len na elementy vramci htmlka
  danej komponenty.
- @ContentChild() sa da napojit na element, ktore su preposlane
  do rodicovskej komponenty cez ng-content.

#### Validacia formularu

- `minLength` pomocou tohto atributu viem definovat minimalny pocet vstupnych znakov v inpute.
  Ak tato podmienka nie je splnena validacia naplni objekt vlastnyNazov.errors.minLength vrati hodnotu true
- `maxLength` pomocou tohto atributu viem definovat maximalny pocet vstupnych znakov v inpute. Ak tato podmienka nie je splnena nedovoli napisat viac znakov do inputu.
- `pattern` sluzi na definovanie vlastnej validacie vstupnych znakov

```html
<form>
	<div class="form-group">
		<label for="firstName">First Name</label>
		<input
			minlength="3"
			maxlength="10"
			pattern="banana"
			ngModel
			name="firstName"
			#firstName="ngModel"
			id="firstName"
			(change)="log(firstName)"
			type="text"
			class="form-control"
			required
		/>
		<div
			*ngIf="firstName.touched && !firstName.valid"
			class="alert alert-danger"
		>
			<div *ngIf="firstName.errors.required">First name is required!</div>
			<div *ngIf="firstName.errors.minlength">
				First name should be minimum {{
				firstName.errors.minlength.requiredLength }} characters!
			</div>
			<div *ngIf="firstName.errors.pattern">
				First name doesn't match the pattern!
			</div>
		</div>
	</div>
</form>
```

#### Stylovanie nevalidnych inputov

- Angular pri nevalidnych formularoch s validaciou pridava specialne classy, ktore mozem pouzivat na stylovanie, napr:

```css
.form-control.ng-touched.ng-invalid {
	border: 2px solid red;
}
```

#### ngForm

- Vytvara instanciu formGroup na najvyssej urovni a sleduje vsetky
  hodnoty formulara a validaciu.

```html
<form #f="ngForm" (ngSubmit)="submit(f)">
	<div class="form-group">
		<label for="firstName">First Name</label>
		<input
			minlength="3"
			maxlength="10"
			pattern="banana"
			ngModel
			name="firstName"
			#firstName="ngModel"
			id="firstName"
			(change)="log(firstName)"
			type="text"
			class="form-control"
			required
		/>
		<div
			*ngIf="firstName.touched && !firstName.valid"
			class="alert alert-danger"
		>
			<div *ngIf="firstName.errors.required">First name is required!</div>
			<div *ngIf="firstName.errors.minlength">
				First name should be minimum {{
				firstName.errors.minlength.requiredLength }} characters!
			</div>
			<div *ngIf="firstName.errors.pattern">
				First name doesn't match the pattern!
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="comment">Comment</label>
		<textarea
			ngModel
			name="comment"
			cols="30"
			rows="10"
			id="comment"
			type="text"
			class="form-control"
		></textarea>
	</div>
	<button class="btn btn-primary">Submit</button>
</form>
```

[link](https://angular.io/api/forms/NgForm)

#### ngModelGroup

- ngModelGroup vytvara a viaze instanciu formGroup na DOM element, tym padom viem zistit ci
  dany formular je celkovo validny alebo nie.

```html
<form #f="ngForm" (ngSubmit)="submit(f)">
	<div *ngIf="!nameCtrl.valid">Invalid</div>
	<div ngModelGroup="name" #nameCtrl="ngModelGroup">
		<input
			minlength="2"
			maxlength="12"
			[ngModel]="name.first"
			name="first"
			type="text"
		/>
		<input [ngModel]="name.last" name="last" type="text" required />
	</div>
	<button class="btn btn-primary">Submit</button>
</form>
```

```tsx
import { Component } from '@angular/core'
import { NgForm } from '@angular/forms'

interface nameType {
	first: string last: string
}

@Component({
selector: 'contact-form', templateUrl: './contact-form.component.html',
styleUrls: ['./contact-form.component.scss'],
})

export class ContactFormComponent {
	name: nameType = { first: 'Janko', last: 'Traktorista' }

submit(f: NgForm) {
	console.log(f.value)
	console.log(f.valid)
	}
}
```

#### Disablednuty stav pre button

```html
<form #f="ngForm" (ngSubmit)="submit(f)">
	<div class="form-group">
		<label for="firstName">First Name</label>
		<input
			minlength="3"
			maxlength="10"
			pattern="banana"
			ngModel
			name="firstName"
			#firstName="ngModel"
			id="firstName"
			(change)="log(firstName)"
			type="text"
			class="form-control"
			required
		/>
		<div
			*ngIf="firstName.touched && !firstName.valid"
			class="alert alert-danger"
		>
			<div *ngIf="firstName.errors.required">First name is required!</div>
			<div *ngIf="firstName.errors.minlength">
				First name should be minimum {{
				firstName.errors.minlength.requiredLength }} characters!
			</div>
			<div *ngIf="firstName.errors.pattern">
				First name doesn't match the pattern!
			</div>
		</div>
	</div>
	<button class="btn btn-primary" [disabled]="!f.valid">Submit</button>
</form>
```

#### Checkbox

```html
<form #f="ngForm" (ngSubmit)="submit(f)">
	<div class="checkbox">
		<label>
			<input ngModel name="isSubscribed" type="checkbox" />Subscribed to mailing
			list.</label
		>
	</div>
</form>
```

#### Select

```html
<form #f="ngForm" (ngSubmit)="submit(f)">
	<div class="form-group">
		<label for="contactMetod"></label>
		<select
			multiple
			ngModel
			name="contactMetod"
			id="contactMetod"
			class="form-control"
		>
			<option *ngFor="let metod of contactMetods" [value]="metod.id">
				{{ metod.name }}
			</option>
			<!-- ak chcem vidiet cely objekt, vytahujem ho cez ngValue -->
			<!-- <option *ngFor="let metod of contactMetods" [ngValue]="metod">
        {{ metod.name }}
      </option> -->
		</select>
	</div>
</form>
```

#### Radio button

```html
<form #f="ngForm" (ngSubmit)="submit(f)">
	<div class="radio">
		<label>
			<input type="radio" name="contactMetod" value="2" ngModel />Phone
		</label>
	</div>
	<p>{{ f.value | json }}</p>
	<br />
	<h3>Select your title, please!</h3>
	<div *ngFor="let title of titles">
		<input type="radio" name="titles" value="title.id" />
		<label for="titles">{{ title.name }}</label>
	</div>
</form>
```

### Reactive forms

- Ak chceme vytvarat vlastnu validaciu a viacej
  kontrolovat validaciu nad formularom a mat vacsi dosah na jeho schopnosti alebo
  napisat vlastne schopnosti, mozem vyuzit od Angularu `ReactiveFormsModule`
- Najskor si importnem ReactiveFormModule

```tsx
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { AppRoutingModule } from './app-routing.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AppComponent } from './app.component'
import { SignUpFormComponent } from './sign-up-form/sign-up-form.component'

@NgModule({
	declarations: [AppComponent, SignUpFormComponent],
	imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
```

```html
<form [formGroup]="form">
	<div class="form-group">
		<label for="username">User Name</label>
		<input
			type="text"
			id="username"
			class="form-control"
			formControlName="username"
		/>
		<div
			class="alert alert-danger"
			*ngIf="username.touched && username.invalid"
		>
			<div *ngIf="username.errors.required">User name is required!</div>
			<div *ngIf="username.errors.minlength">
				User name should be minimum {{ username.errors.minlength.requiredLength
				}} characters!
			</div>
			<div *ngIf="username.errors.cannotContainSpace">
				User name cannot contain space!
			</div>
		</div>
	</div>
	<button class="btn btn-primary" type="submit">Sign up</button>
</form>
```

- Priklad vytvorenia vlastnej validacie pre username, kde kontrolujem ci sa
  v inpute nachadza prazdny priestor. Ak sa nachadza zobrazi sa nam chybova hlaska
  vdaka premennej `cannotContainSpace`

```tsx
import { AbstractControl, ValidationErrors } from '@angular/forms'
export class UserNameValidators {
	static cannotContainSpace(control: AbstractControl): ValidationErrors | null {
		if ((control.value as string).indexOf(' ') >= 0) {
			return { cannotContainSpace: true }
			// return {
			// minlength: { requiredLength: 10, actualLength:control.value.length },
			// };
		}
		return null
	}
}
```

```tsx
import { Component } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { UserNameValidators } from './username.validators'

@Component({
	selector: 'sign-up-form',
	templateUrl: './sign-up-form.component.html',
	styleUrls: ['./sign-up-form.component.scss'],
})
export class SignUpFormComponent {
	form = new FormGroup({
		username: new FormControl('', [
			Validators.required,
			Validators.minLength(3),
			UserNameValidators.cannotContainSpace,
		]),
	})
	// Vytvorili sme si premennu pre kratsi zapis vytahovania validacie a errors stavov pre username

	get username() {
		return this.form.get('username')
	}
}
```

- Priklad async validacie

```html
<form [formGroup]="form" (ngSubmit)="login()">
	<div class="alert alert-danger" *ngIf="form.errors">
		The user name or password is invalid!
	</div>
	<div formGroupName="account">
		<div class="form-group">
			<label for="username">User Name</label>
			<input
				type="text"
				id="username"
				class="form-control"
				formControlName="username"
			/>
			<div *ngIf="username.pending">Checking for uniqueness...</div>
			<div
				class="alert alert-danger"
				*ngIf="username.touched && username.invalid"
			>
				<div *ngIf="username.errors.cannotContainSpace">
					User name cannot contain space!
				</div>
				<div *ngIf="username.errors.shouldBeJanko">
					User name is alredy taken!
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input
				type="password"
				id="password"
				class="form-control"
				formControlName="password"
			/>
			<div *ngIf="password.pending">Checking...</div>
			<div
				class="alert alert-danger"
				*ngIf="password.touched && password.invalid"
			>
				<div *ngIf="password.errors.mustIncludes">
					Password should contains at least two numbers, one uppercase letter
					and one lowercase letter!
				</div>
				<div *ngIf="password.errors.mustBeValue">Invalid password!</div>
			</div>
		</div>
	</div>
	<button class="btn btn-primary" type="submit">Sign up</button>
</form>
```

```tsx
import { Component } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { UserPasswordValidator } from './password.validators'
import { UserNameValidators } from './username.validators'

@Component({
	selector: 'sign-up-form',
	templateUrl: './sign-up-form.component.html',
	styleUrls: ['./sign-up-form.component.scss'],
})
export class SignUpFormComponent {
	form = new FormGroup({
		//prvy parameter: '', druhy parameter: [sync funkcie], treti parameter: [async funkcie]
		account: new FormGroup({
			username: new FormControl(
				'',
				UserNameValidators.cannotContainSpace,
				UserNameValidators.shouldBeJanko
			),
			password: new FormControl(
				'',
				UserPasswordValidator.mustIncludes,
				UserPasswordValidator.mustBeValue
			),
		}),
	})

	login() {
		// let isValid = authService.login(this.form.value);
		// nastavenie erroru vramci validacie
		this.form.setErrors({ invalidLogin: true })
	}

	get username() {
		return this.form.get('account.username')
	}
	get password() {
		return this.form.get('account.password')
	}
}

//https://stackoverflow.com/questions/52363204/angular-6-settimeout-function-is-not-working-in-my-custom-validator
```

##### Custom validacia

```tsx
import { AbstractControl, ValidationErrors } from '@angular/forms'

export class UserPasswordValidator {
	static mustIncludes(control: AbstractControl): ValidationErrors | null {
		const pattern = new RegExp(
			'^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]{2})[0-9a-zA-Z]{3,10}'
		)

		if (!pattern.test(control.value)) {
			return { mustIncludes: true }
		}
		return null
	}
	static mustBeValue(
		control: AbstractControl
	): Promise<ValidationErrors | null> {
		const result = new Promise(resolve => {
			setTimeout(() => {
				console.log(control.value)
				if (control.value !== '12Ab34') {
					resolve({ mustBeValue: true })
				} else {
					resolve(null)
				}
			}, 500)
		})
		console.log(result)

		return result
	}
}
```

```tsx
import { AbstractControl, ValidationErrors } from '@angular/forms'

export class UserNameValidators {
	static cannotContainSpace(control: AbstractControl): ValidationErrors | null {
		if ((control.value as string).indexOf(' ') >= 0) {
			return { cannotContainSpace: true }
			// return {
			//   minlength: { requiredLength: 10, actualLength: control.value.length },
			// };
		}
		return null
	}
	static shouldBeJanko(
		control: AbstractControl
	): Promise<ValidationErrors | null> {
		const result = new Promise(resolve => {
			setTimeout(() => {
				console.log(control.value)

				if (control.value === 'janko') {
					resolve({ shouldBeJanko: true })
				} else {
					resolve(null)
				}
			}, 500)
		})
		console.log(result)

		return result
	}
}
```

#### Form Array - removeAt()

```html
<form>
	<input
		type="text"
		class="form-control"
		(keyup.enter)="addTopic(topic)"
		#topic
	/>
	<ul class="list-group">
		<li
			class="list-group-item"
			*ngFor="let topic of topics.controls"
			(click)="removeTopic(topic)"
		>
			{{ topic.value }}
		</li>
	</ul>
</form>
```

```tsx
import { Component } from '@angular/core'
import { FormArray, FormControl, FormGroup } from '@angular/forms'

@Component({
	selector: 'new-course-form',
	templateUrl: './new-course-form.component.html',
	styleUrls: ['./new-course-form.component.scss'],
})
export class NewCourseFormComponent {
	form = new FormGroup({ topics: new FormArray([]) })

	addTopic(topic: HTMLInputElement) {
		this.topics.push(new FormControl(topic.value))
		topic.value = ''
	}

	get topics() {
		return this.form.get('topics') as FormArray
	}

	removeTopic(topic: FormControl) {
		let index = this.topics.controls.indexOf(topic)
		this.topics.removeAt(index)
	}
}
```

#### Priklad generovania elementov s akciou remove daneho elementu

```html
<form>
	<input
		type="text"
		class="form-control"
		(keyup.enter)="addTopic(topic)"
		#topic
	/>
</form>
<ul class="list-group">
	<li
		class="list-group-item"
		*ngFor="let topic of topics.controls; let i = index"
	>
		{{ topic.value }}
		<button (click)="removeTopic(i)">Remove</button>
	</li>
</ul>
```

```tsx
import { Component } from '@angular/core'
import { FormArray, FormControl, FormGroup } from '@angular/forms'

@Component({
	selector: 'my-form',
	templateUrl: './my-form.component.html',
	styleUrls: ['./my-form.component.scss'],
})
export class MyFormComponent {
	form = new FormGroup({
		topics: new FormArray([]),
	})

	addTopic(topic: HTMLInputElement) {
		this.topics.push(new FormControl(topic.value))
		topic.value = ''
	}

	removeTopic(index: number) {
		this.topics.removeAt(index)
	}

	get topics() {
		return this.form.get('topics') as FormArray
	}
}
```

#### Generovanie formularu s viacerymi vstupmi a s moznostou mazania prvku v poli

```html
<form [formGroup]="addressForm" (ngSubmit)="save()">
	<div formArrayName="addressList">
		<h3>Addresses</h3>
		<button (click)="addAddress()">Add new address!</button>
		<div *ngFor="let address of addresses.controls; let i = index">
			Address {{ i + 1 }}
			<div class="form-container" [formGroup]="address">
				<div class="flex-50">
					<input formControlName="streetAddress" />
				</div>
				<div class="flex-50">
					<input formControlName="city" />
				</div>
				<div class="flex-50">
					<input formControlName="state" />
				</div>
				<div class="flex-30">
					<input formControlName="zip" />
				</div>
				<button type="button" class="flex-20" (click)="removeAddressAt(i)">
					Remove
				</button>
			</div>
		</div>
	</div>
	<button type="submit" class="flex-20">Save</button>
</form>
<h2>AddressList</h2>
<div *ngFor="let address of saveData?.addressList">
	<div>Street: {{ address.streetAddress }}</div>
	<div>City: {{ address.city }}</div>
	<div>State: {{ address.state }}</div>
	<div>Zip: {{ address.zip }}</div>
	<hr />
</div>
```

```tsx
import { Component } from '@angular/core'
import {
	FormArray,
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from '@angular/forms'

@Component({
	selector: 'form-builder',
	templateUrl: './form-builder.component.html',
	styleUrls: ['./form-builder.component.scss'],
})
export class FormBuilderComponent {
	//toto je to iste ako zapis dole:
	// form = new FormGroup({
	//   name: new FormControl('', Validators.required),
	//   contact: new FormGroup({
	//     email: new FormControl(),
	//     phone: new FormControl(),
	//   }),
	//   topics: new FormArray([]),
	// });

	//zapis formBuilderu
	addressForm: FormGroup
	saveData: any

	constructor(private fb: FormBuilder) {
		this.addressForm = fb.group({
			addressList: fb.array([]),
		})
		this.addAddress()
	}

	get addresses() {
		return this.addressForm.get('addressList') as FormArray
	}

	addAddress() {
		this.addresses.push(this.createAddress())
	}

	createAddress() {
		return this.fb.group({ streetAddress: [], city: [], state: [], zip: [] })
	}

	save() {
		this.saveData = this.addressForm.value
	}

	removeAddressAt(i) {
		this.addresses.removeAt(i)
		this.saveData.addressList.splice(i, 1)
	}
}
```

### Konzumovanie Http services

#### Getting data

- V tomto priklade sme pouzili starsi balicek `@angular/http`, ktory je nutne vramci App.module.ts definovat ako import `HttpModule`

```html
<ul class="list-group">
	<li class="list-group-item" *ngFor="let post of posts">{{ post.title }}t</li>
</ul>
```

```tsx
import { Component } from '@angular/core'
import { Http } from '@angular/http'

@Component({
	selector: 'post-component',
	templateUrl: './post-component.component.html',
	styleUrls: ['./post-component.component.scss'],
})
export class PostComponentComponent {
	posts: any[]
	private url: string = 'http://jsonplaceholder.typicode.com/posts'
	constructor(private http: Http) {
		http.get(this.url).subscribe(response => {
			this.posts = response.json()
		})
	}
}
```

#### Creating data

```html
<input
	type="text"
	class="form-control"
	#title
	(keyup.enter)="createPost(title)"
/>
<ul class="list-group">
	<li class="list-group-item" *ngFor="let post of posts">{{ post.title }}</li>
</ul>
```

```tsx
import { Component } from '@angular/core'
import { Http } from '@angular/http'

@Component({
	selector: 'post-component',
	templateUrl: './post-component.component.html',
	styleUrls: ['./post-component.component.scss'],
})
export class PostComponentComponent {
	posts: any[]
	private url: string = 'http://jsonplaceholder.typicode.com/posts'
	constructor(private http: Http) {
		http.get(this.url).subscribe(response => {
			this.posts = response.json()
		})
	}

	createPost(input: HTMLInputElement) {
		let post = { title: input.value }
		input.value = ''
		this.http.post(this.url, JSON.stringify(post)).subscribe(response => {
			post['id'] = response.json().id
			this.posts.splice(0, 0, post)
			console.log(response.json())
		})
	}
}
```

#### Updating data

- Pri updatovani dat sa pouzivaju dve http metody a to `patch()` alebo `put()`
- Patch() sluzi na editaciu viacerych premennych vramci jedneho objektu
- Put() sluzi na zmenu jednej premennej `

```html
<input
	type="text"
	class="form-control"
	#title
	(keyup.enter)="createPost(title)"
/>
<ul class="list-group">
	<li class="list-group-item" *ngFor="let post of posts">
		<button class="btn btn-default btn-sm" (click)="updatePost(post)">
			Update
		</button>
		{{ post.title }}
	</li>
</ul>
```

```tsx
import { Component } from '@angular/core'
import { Http } from '@angular/http'

@Component({
	selector: 'post-component',
	templateUrl: './post-component.component.html',
	styleUrls: ['./post-component.component.scss'],
})
export class PostComponentComponent {
	posts: any[]
	private url: string = 'http://jsonplaceholder.typicode.com/posts'
	constructor(private http: Http) {
		http.get(this.url).subscribe(response => {
			this.posts = response.json()
		})
	}

	createPost(input: HTMLInputElement) {
		let post = { title: input.value }
		input.value = ''
		this.http.post(this.url, JSON.stringify(post)).subscribe(response => {
			post['id'] = response.json().id
			this.posts.splice(0, 0, post)
			console.log(response.json())
		})
	}

	updatePost(post) {
		this.http
			.patch(this.url + '/' + post.id, JSON.stringify({ isRead: true }))
			.subscribe(response => {
				console.log(response.json())
			})
	}
}
```

#### Deleting data

```html
<input
	type="text"
	class="form-control"
	#title
	(keyup.enter)="createPost(title)"
/>
<ul class="list-group">
	<li class="list-group-item" *ngFor="let post of posts">
		<button class="btn btn-default btn-sm" (click)="updatePost(post)">
			Update
		</button>
		<button class="btn btn-default btn-sm" (click)="deletePost(post)">
			Delete
		</button>
		{{ post.title }}
	</li>
</ul>
```

```tsx
import { Component } from '@angular/core'
import { Http } from '@angular/http'

@Component({
	selector: 'post-component',
	templateUrl: './post-component.component.html',
	styleUrls: ['./post-component.component.scss'],
})
export class PostComponentComponent {
	posts: any[]
	private url: string = 'http://jsonplaceholder.typicode.com/posts'
	constructor(private http: Http) {
		http.get(this.url).subscribe(response => {
			this.posts = response.json()
		})
	}

	createPost(input: HTMLInputElement) {
		let post = { title: input.value }
		input.value = ''
		this.http.post(this.url, JSON.stringify(post)).subscribe(response => {
			post['id'] = response.json().id
			this.posts.splice(0, 0, post)
			console.log(response.json())
		})
	}

	updatePost(post) {
		this.http
			.patch(this.url + '/' + post.id, JSON.stringify({ isRead: true }))
			.subscribe(response => {
				console.log(response.json())
			})
	}

	deletePost(post) {
		this.http.delete(this.url + '/' + post.id).subscribe(() => {
			let index = this.posts.indexOf(post)
			this.posts.splice(index, 1)
		})
	}
}
```

#### ngOnInit()

- Je to zivotny cyklus, ktory sa vola po tom ako Angular inicializuje vsetky viazane vlastnosti na udaje. V tejto metode by sme mali definovat vsetky inicializacne ulohy na spracovanie (nacitanie dat z backendu pomocou metody get()).

```tsx
import { Component, OnInit } from '@angular/core'
import { Http } from '@angular/http'
@Component({
	selector: 'post-component',
	templateUrl: './post-component.component.html',
	styleUrls: ['./post-component.component.scss'],
})
export class PostComponentComponent implements OnInit {
	posts: any[]
	private url: string = 'http://jsonplaceholder.typicode.com/posts'
	constructor(private http: Http) {}
	createPost(input: HTMLInputElement) {
		let post = { title: input.value }
		input.value = ''
		this.http.post(this.url, JSON.stringify(post)).subscribe(response => {
			post['id'] = response.json().id
			this.posts.splice(0, 0, post)
			console.log(response.json())
		})
	}
	updatePost(post) {
		this.http
			.patch(
				this.url + '/' + post.id,
				JSON.stringify({
					isRead: true,
				})
			)
			.subscribe(response => {
				console.log(response.json())
			})
	}
	deletePost(post) {
		this.http.delete(this.url + '/' + post.id).subscribe(() => {
			let index = this.posts.indexOf(post)
			this.posts.splice(index, 1)
		})
	}
	ngOnInit() {
		this.http.get(this.url).subscribe(response => {
			this.posts = response.json()
		})
	}
}
```

#### Tvorba service

```
ng g s nazovService
```

```tsx
import { Injectable } from '@angular/core'
import { Http } from '@angular/http'

@Injectable({
	providedIn: 'root',
})
export class PostService {
	private url: string = 'http://jsonplaceholder.typicode.com/posts'

	constructor(private http: Http) {}

	getPosts() {
		return this.http.get(this.url)
	}

	cratePost(post) {
		return this.http.post(this.url, JSON.stringify(post))
	}

	updatePost(post) {
		return this.http.patch(
			this.url + '/' + post.id,
			JSON.stringify({ isRead: true })
		)
	}

	deletePost(id) {
		return this.http.delete(this.url + '/' + id)
	}
}
```

- Pouzitie v komponente

```tsx
import { Component, OnInit } from '@angular/core'
import { PostService } from '../post.service'

@Component({
	selector: 'post-component',
	templateUrl: './post-component.component.html',
	styleUrls: ['./post-component.component.scss'],
})
export class PostComponentComponent implements OnInit {
	posts: any[]

	constructor(private service: PostService) {}

	createPost(input: HTMLInputElement) {
		let post = { title: input.value }
		input.value = ''
		this.service.cratePost(post).subscribe(response => {
			post['id'] = response.json().id
			this.posts.splice(0, 0, post)
			console.log(response.json())
		})
	}

	updatePost(post) {
		this.service.updatePost(post).subscribe(response => {
			console.log(response.json())
		})
	}

	deletePost(post) {
		this.service.deletePost(post.id).subscribe(() => {
			let index = this.posts.indexOf(post)
			this.posts.splice(index, 1)
		})
	}

	ngOnInit() {
		this.service.getPosts().subscribe(response => {
			this.posts = response.json()
		})
	}
}
```

#### Handlovanie neocakavanych errorov

```tsx
  ngOnInit() {
    this.service.getPosts().subscribe(
      (response) => {
        this.posts = response.json();
      },
      (error) => {
        alert('An unexpected error occurred!');
        console.log(error);
      }
    );
  }
```

#### Handlovanie ocakavanych errorov

```tsx
import { Component, OnInit } from '@angular/core'
import { PostService } from '../post.service'

@Component({
	selector: 'post-component',
	templateUrl: './post-component.component.html',
	styleUrls: ['./post-component.component.scss'],
})
export class PostComponentComponent implements OnInit {
	posts: any[]

	constructor(private service: PostService) {}

	createPost(input: HTMLInputElement) {
		let post = { title: input.value }
		input.value = ''
		this.service.cratePost(post).subscribe(
			response => {
				post['id'] = response.json().id
				this.posts.splice(0, 0, post)
				console.log(response.json())
			},
			(error: Response) => {
				if (error.status === 400) {
					alert(error.json())
				} else {
					alert('An unexpected error occurred!')
				}
			}
		)
	}

	updatePost(post) {
		this.service.updatePost(post).subscribe(
			response => {
				console.log(response.json())
			},
			(error: Response) => {
				if (error.status === 400) {
					alert(error.json())
				} else {
					alert('An unexpected error occurred!')
				}
			}
		)
	}

	deletePost(post) {
		this.service.deletePost(post.id).subscribe(
			() => {
				let index = this.posts.indexOf(post)
				this.posts.splice(index, 1)
			},
			(error: Response) => {
				if (error.status === 404) {
					alert('This post has already been deleted!')
				} else {
					alert('An unexpected error occurred!')
				}
			}
		)
	}

	ngOnInit() {
		this.service.getPosts().subscribe(
			response => {
				this.posts = response.json()
			},
			error => {
				alert('An unexpected error occurred!')
				console.log(error)
			}
		)
	}
}
```

### Routing a navigacia

Vramci suboru app.module.ts je potrebne v `imports` definovat `RouterModule.forRoot()`
(aby Angular zaregistroval nas routing a mali sme dostupnu tuto funkcionalitu) a nasledne vo vnutri v `[]` definovat objekt s premennou `path` a `component`. V tomto priklade do `forRoot()` vkladame premennu `const appRoutes`, ktoru sme si definovali na zaciatku suboru.

```tsx
import { RouterModule, Routes } from '@angular/router'

const appRoutes: Routes = [
	{ path: '', component: HomeComponent },
	{
		path: 'users',
		component: UsersComponent,
		children: [{ path: ':id/:name', component: UserComponent }],
	},
	{
		path: 'servers',
		component: ServersComponent,
		children: [
			{ path: ':id', component: ServerComponent },
			{ path: ':id/edit', component: EditServerComponent },
		],
	},
]

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		UsersComponent,
		ServersComponent,
		UserComponent,
		EditServerComponent,
		ServerComponent,
	],
	imports: [BrowserModule, FormsModule, RouterModule.forRoot(appRoutes)],
	providers: [ServersService],
	bootstrap: [AppComponent],
})
export class AppModule {}
```

#### Router-outlet

Na to aby Angular vedel, kam chceme nase routy vykreslit sa pouziva direktiva `<router-outlet><router-outlet>`(aj ked zapis router-outletu vyzera ako zapis komponenty, je to direktiva. Direktivy mozumat zapis vyzerajuci ako komponenty.)

(cast kodu z app.component.html)

```html
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">
		<router-outlet></router-outlet>
	</div>
</div>
```

#### routerLink

Aby sme sa vedeli navigovat z jedneho elementu na druhy bez toho, aby sa nam refreshla stranka, vyuzivame namiesto atributu `href=""` atribut `routerLink=""`. Mame niekolko sposobov ako routerLink
zapisovat:

```html
<ul class="nav nav-tabs">
	<li role="presentation"><a routerLink="/">Home</a></li>
	<li role="presentation"><a [routerLink]="'/servers'">Servers</a></li>
	<li role="presentation"><a [routerLink]="['/users']">Users</a></li>
</ul>
```

##### Absolutna a relativna cesta v routerLinku

Do routerLink uvadzame cestu, podla ktorej routerLink vie, kam nas ma prezmerovat. Zadefinovat
vieme `absolutnu` a `relativnu` cestu.

- Absolutna cesta zacina s `/` a je vzdy napojena priamo na root domain

```html
<a routerLink="/servers">Reload Page</a>
```

- Relativna cesta zacina bez `/` alebo s `./` a je napojena na aktualnu cestu. Tiez moze zacinat s `../` co znamena, ze chceme aby isla o uroven (alebo viac urovni) vyssie z aktualnej cesty.

```html
<a routerLink="servers">Reload Page</a>
```

```html
<a routerLink="./servers">Reload Page</a>
```

```html
<a routerLink="../servers">Reload Page</a>
```

```html
<a routerLink="../../servers">Reload Page</a>
```

#### routerLinkActive

Pomaha nam pomocou nastylovania zobrazit, ktory element sa klikol. `routerLinkActive="active"` analyzuje prave nacitanu cestu a potom skontroluje, ktory link vedie k route, ktora pouziva tuto
cestu. Pri nasej Home stranke ale mame routerLink="/". V takomto pripade, by tato stranka stale ostala vyznacena ako aktivna aj keby sme sa preklikli niekam inam, lebo jej cesta je "/" (takto zacina kazda cesta), preto aby sme odstranili toto neziaduce spravanie vyuzivame `[routerLinkActiveOptions]="{ exact: true }"`, vdaka tomu sa nam Home Page vyznaci iba vtedy, ked nasa cela cesta bude "/" (a nie iba vtedy ked "/" bude len sucastou linku)

```html
<ul class="nav nav-tabs">
	<li
		role="presentation"
		routerLinkActive="active"
		[routerLinkActiveOptions]="{ exact: true }"
	>
		<a routerLink="/">Home</a>
	</li>
	<li role="presentation" routerLinkActive="active">
		<a routerLink="/servers">Servers</a>
	</li>
	<li role="presentation" routerLinkActive="active">
		<a [routerLink]="['/users']">Users</a>
	</li>
</ul>
```

#### Navigovane pomocou metody (Navigating Programmatically)

Do suboru sme si pridali button, v ktorom volame pomocou click eventu metodu
`onLoadServers()`

(subor home.component.html)

```html
<h4>Welcome to Server Manager 4.0</h4>
<p>Manage your Servers and Users.</p>
<button class="btn btn-primary" (click)="onLoadServers()">Load Servers</button>
```

Do contructora pridavame `router`, nesmieme zabudnut na `import routeru`. Vo funkcii ` onLoadServers()` pouzivame metodu `navigate([])`. Vo vnutri navigate:(['prvyElementNasejCesty',])

(subor home.component.ts)

```tsx
import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
	constructor(private router: Router) {}

	ngOnInit() {}

	onLoadServers() {
		//complex calculation
		this.router.navigate(['/servers'])
	}
}
```

##### Pouzivanie relativnej cesty pri navigovani pomocou metody (Navigating Programmatically)

V subore si vytvorime button, v ktorom pomocou click eventu budeme volat metodu `onReload()`

(subor servers.component.html)

```html
<div class="row">
	<div class="col-xs-12 col-sm-4">
		<div class="list-group">
			<a
				[routerLink]="['/servers', server.id]"
				[queryParams]="{ allowEdit: server.id === 3 ? '1' : '0' }"
				fragment="loading"
				href="#"
				class="list-group-item"
				*ngFor="let server of servers"
			>
				{{ server.name }}
			</a>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4">
		<router-outlet></router-outlet>
		<button class="btn btn-primary" (click)="onReload()">Reload Page</button>
		<app-edit-server></app-edit-server>
		<hr />
		<app-server></app-server>
	</div>
</div>
```

Do constructore pridavame `router` a tak isto ho musime aj `importnut`.
V metode `onReload()` vyuzivame metodu `navigate()`. Tato metoda narozdiel od routerLinku nevie na ktorom route sa prave nachadzame, takze jej to musime povedat. Na to sa vyuziva `druhy argument` vo funkcii navigate().
Druhy argument bude javascript objekt `{relativeTo: this.route}` , kde definujeme ku ktoremu routu by mal byt tento link napojeny. Aby sme sa dostali ku aktualnemu routu, musime si importnut a do construktora pridat `ActivatedRoute`. S tymto nastavenim angular bude mat potrebnu informaciu o
aktualnom route.

(subor servers.component.ts)

```tsx
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ServersService } from './servers.service'

@Component({
	selector: 'app-servers',
	templateUrl: './servers.component.html',
	styleUrls: ['./servers.component.css'],
})
export class ServersComponent implements OnInit {
	public servers: { id: number; name: string; status: string }[] = []

	constructor(
		private serversService: ServersService,
		private router: Router,
		private route: ActivatedRoute
	) {}

	ngOnInit() {
		this.servers = this.serversService.getServers()
	}

	onReload() {
		this.router.navigate(['servers'], { relativeTo: this.route })
	}
}
```

#### Preposielanie parametrov routam

Do cesty vieme pridat aj dynamicke prvky. To, ze sa jedna o dynamicku cast cesty definuju `:`

(subor app.module.ts)

```tsx
const appRoutes: Routes = [{ path: 'users/:id', component: UsersComponent }]
```

Ak by sme chceli pridat okrem `id` aj dalsi
dynamicky prvok, napr `name`, zapis by vyzeral takto:

(subor app.module.ts)

```tsx
const appRoutes: Routes = [
	{ path: 'users/:id/:name', component: UsersComponent },
]
```

#### Fetchovanie routovych parametrov

Chceme mat pristup k datam, ktore nam pouzivatel poslal (napriklad chceme mat dostupne
id a name)

(subor user.component.html)

```html
<p>User with ID _ID_ loaded.</p>
<p>User name is _NAME_</p>
```

V javascriptovom subore si musime opat pridat do constructora aktualnu routu. Nadefinujeme si zaciatocny stav pre nasho usera a do metody ngOnInit() vytvorime logiku, ako sa dostat k id a k name usera.

(subor user.component.ts)

```tsx
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { Subscription } from 'rxjs'

@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
	user: { id: number; name: string }

	constructor(private route: ActivatedRoute) {}

	ngOnInit() {
		this.user = {
			id: this.route.snapshot.params['id'],
			name: this.route.snapshot.params['name'],
		}
}
```

Teraz uz hodnotu `id` a `name` nemame natvrdo napisanu v kode, ale k hodnote sa dostavame tak, ze `z objektu user vytahujeme parametre` id a name.

(subor user.component.html)

```html
<p>User with ID {{ user.id }} loaded.</p>
<p>User name is {{ user.name }}</p>
```

##### Fetchovanie routovych parametrov reaktivne

Po nacitani stranky je na stranke zobrazene nase id, ktore je 3 a nase meno, ktore je Max. Tieto
udaje su aj v url adrese. Po kliknuti na link sa udaje v url adrese prepisu, teraz nase id bude 10 a meno Anna. Na stranke vsak stale ostava zobrazeny Max s jeho id 3. Nie je to chyba, je to defaultne nastavenie Angularu. Nasa data sme si nacitali pouzitim snapshot objektu na route. Tym, ze klikneme na linku s Annou sa vsak nase data zobrazene na stranke nenacitaju, pretoze Angular uz ma informaciu o tom, ze sa na danej komponente nachadzame. Nove data by sme dostali len ak by sme na danej komponente predtym nikdy neboli. Angular nebude vykreslovat komponentu na ktorej sa predsa uz nachadza. To, ze my mame nove data, ktore potrebujeme nacitat on nevie. Pre prve nacitanie
udajov je ok pouzit `snapshot`, ale ak chceme mat dosah na nove data aj po tom, co sme uz boli na danej komponente, na to potrebujeme iny sposob ako sa k tym datam dostat. Z route objektu budeme vytahovat `params` a kedze pojde o asynchornnu funkciu, vyuzijeme `subscribe()` metodu aby sme nas kod neblokovali. vdaka tomuto zapisu sa nam updatne nas user objekt vzdy ked sa zmeni parameter. Tento kod sa nevykona ked sa spusti ngOnInit, vykona sa iba ked sa nam zmenia parametre. Vdaka tomuto zapisu sa nam po kliknuti na link s Annou prepisu udaje na stranke z Maxa na Annu.

```html
<p>User with ID {{ user.id }} loaded.</p>
<p>User name is {{ user.name }}</p>
<hr />
<a [routerLink]="['/users', 10, 'Anna']">Load Anna (10)</a>
```

(subor user.component.ts)

```tsx
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { Subscription } from 'rxjs'

@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
	user: { id: number; name: string }

	constructor(private route: ActivatedRoute) {}

	ngOnInit() {
		this.user = {
			id: this.route.snapshot.params['id'],
			name: this.route.snapshot.params['name'],
		}
		//vdaka tomuto zapisu sa nam updatne nas user objekt vzdy ked sa zmeni parameter. Tento kod sa nevykona ked sa spusti ngOnInit, vykona sa iba ked sa nam zmenia parametre
		this.route.params.subscribe((params: Params) => {
			this.user.id = params['id']
			this.user.name = params['name']
		})
	}
}
```

Ak by sme chceli `zrusit subscribe()` metodu, vyuzivame na to `unsubscribe()` metodu, ktoru volame v `ngOnDestroy()`.

(subor user.component.ts)

```tsx
import { Component, OnInit, onDestroy } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { Subscription } from 'rxjs'

@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit, onDestroy {
	user: { id: number; name: string }
	paramsSubscription: Subscription

	constructor(private route: ActivatedRoute) {}

	ngOnInit() {
		this.user = {
			id: this.route.snapshot.params['id'],
			name: this.route.snapshot.params['name'],
		}
		this.paramsSubscription = this.route.params.subscribe((params: Params) => {
			this.user.id = params['id']
			this.user.name = params['name']
		})
	}

	ngOnDestroy() {
		this.paramsSubscription.unsubscribe()
	}
}
```

#### Query parametre a fragmenty

Query parametre nie su direktivy, su to len prepojitelne vlastnosti direktivy routerLink. Fragment mozme mat len jeden. Query parametre aj fragment su v tomto pripade volane pomocou typescript kodu:

(cast kodu zo suboru servers.component.html)

```html
<div class="list-group">
	<a
		[routerLink]="['/servers', 5, 'edit']"
		[queryParams]="{ allowEdit: '1'}"
		fragment="loading"
		href="#"
		class="list-group-item"
		*ngFor="let server of servers"
	>
		{{ server.name }}
	</a>
</div>
```

Mozme ich vsak volat pomocou metody `onLoadServer()`, kde do `navigate()` vkladame `queryParams` a `fragment`

(subor home.component.html)

```html
<h4>Welcome to Server Manager 4.0</h4>
<p>Manage your Servers and Users.</p>
<button class="btn btn-primary" (click)="onLoadServer(1)">Load Server 1</button>
```

(subor home.component.ts)

```tsx
import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
	constructor(private router: Router) {}

	ngOnInit() {}

	onLoadServer(id: number) {
		this.router.navigate(['/servers', id, 'edit'], {
			queryParams: { allowEdit: '1' },
			fragment: 'loading',
		})
	}
}
```

Ak chceme mat pristup ku query parametrom a fragmentu, budeme na to potrebovat `ActivatedRoute`. Opat mame dva sposoby ako mat na nich dosah. Bud z route objektu volame `snapshot` (co ale moze sposobit rovnake problemy ako pri priklade s Annou a s params) alebo vyuzijeme `subscribe()` (co nam umozni vidiet aktualne zmeny aj po tom, co sme uz vstupili do komponenty a az tak vykonali dalsie zmeny)

(subor edit-server.component.ts)

```tsx
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'

import { ServersService } from '../servers.service'

@Component({
	selector: 'app-edit-server',
	templateUrl: './edit-server.component.html',
	styleUrls: ['./edit-server.component.css'],
})
export class EditServerComponent implements OnInit {
	server: { id: number; name: string; status: string }
	serverName = ''
	serverStatus = ''

	constructor(
		private serversService: ServersService,
		private route: ActivatedRoute
	) {}

	ngOnInit() {
		console.log(this.route.snapshot.queryParams)
		console.log(this.route.snapshot.fragment)
		this.route.queryParams.subscribe()
		this.route.fragment.subscribe()
		this.server = this.serversService.getServer(1)
		this.serverName = this.server.name
		this.serverStatus = this.server.status
	}

	onUpdateServer() {
		this.serversService.updateServer(this.server.id, {
			name: this.serverName,
			status: this.serverStatus,
		})
	}
}
```

Do query parametrov vieme pridavat aj `logiku`, ktorou napriklad budeme kontrolovat ci user ma pravo editovat server alebo nema (na zaklade id):

(subor servers.component.html)

```html
<div class="row">
	<div class="col-xs-12 col-sm-4">
		<div class="list-group">
			<a
				[routerLink]="['/servers', server.id]"
				[queryParams]="{ allowEdit: server.id === 3 ? '1' : '0' }"
				fragment="loading"
				href="#"
				class="list-group-item"
				*ngFor="let server of servers"
			>
				{{ server.name }}
			</a>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4"><router-outlet></router-outlet></div>
</div>
```

#### Uchovanie query parametrov pomocou queryParamsHandling

```tsx
type QueryParamsHandling = 'merge' | 'preserve' | ''
```

- `merge` : prida nove parametre do aktualnych parametrov
- `preserve` : vdaka preserve nestratime stare parametre

(zo suboru server.component.ts)

```tsx
	onEdit() {
		this.router.navigate(['edit'], {
			relativeTo: this.route,
			queryParamsHandling: 'preserve',
		})
	}
}
```

Opat vyuzivame `subscribe()`, aby sme mali zobrazene aktualne data aj po vstupe do danej komponenty a naslednej zmene.

(subor edit-server.component.ts)

```tsx
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'

import { ServersService } from '../servers.service'

@Component({
	selector: 'app-edit-server',
	templateUrl: './edit-server.component.html',
	styleUrls: ['./edit-server.component.css'],
})
export class EditServerComponent implements OnInit {
	server: { id: number; name: string; status: string }
	serverName = ''
	serverStatus = ''
	allowEdit = false //zaciatocny stav

	constructor(
		private serversService: ServersService,
		private route: ActivatedRoute
	) {}

	ngOnInit() {
		console.log(this.route.snapshot.queryParams)
		console.log(this.route.snapshot.fragment)
		this.route.queryParams.subscribe((queryParams: Params) => {
			this.allowEdit = queryParams['allowEdit'] === '1' ? true : false //logika pre allowEdit
		})
		this.route.fragment.subscribe()
		this.server = this.serversService.getServer(1)
		this.serverName = this.server.name
		this.serverStatus = this.server.status
	}

	onUpdateServer() {
		this.serversService.updateServer(this.server.id, {
			name: this.serverName,
			status: this.serverStatus,
		})
	}
}
```

Ak nemame pravo menit zobrazi sa nam iba title, ak mame pravo menit (allowEdit bude true) zobrazi sa nam ostatny kod

(subor edit-server.component.html)

```html
<h4 *ngIf="!allowEdit">You are not alloved to edit!</h4>
<div *ngIf="allowEdit">
	<div class="form-group">
		<label for="name">Server Name</label>
		<input
			type="text"
			id="name"
			class="form-control"
			[(ngModel)]="serverName"
		/>
	</div>
	<div class="form-group">
		<label for="status">Server Status</label>
		<select id="status" class="form-control" [(ngModel)]="serverStatus">
			<option value="online">Online</option>
			<option value="offline">Offline</option>
		</select>
	</div>
	<button class="btn btn-primary" (click)="onUpdateServer()">
		Update Server
	</button>
</div>
```

#### Child (nested) routes

Na zobrazenie deti je potrebny `<router-outlet></router-outlet>`, aby Angular vedel, kde v kode maju byt zobrazene. Nas ` hlavny router-outlet` v app.component.html totiz zachyti len hlavne routy, nie ich deti.

(zo suboru app.module.ts)

```tsx
const appRoutes: Routes = [
	{ path: '', component: HomeComponent, pathMatch: 'full' },
	{
		path: 'users',
		component: UsersComponent,
		children: [{ path: ':id/:name', component: UserComponent }],
	}
```

(subor users.component.html)

```html
<div class="row">
	<div class="col-xs-12 col-sm-4">
		<div class="list-group">
			<a
				[routerLink]="['/users', user.id, user.name]"
				href="#"
				class="list-group-item"
				*ngFor="let user of users"
			>
				{{ user.name }}
			</a>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4">
		<router-outlet></router-outlet>
		<!-- <app-user></app-user> -->
	</div>
</div>
```

#### Redirecting and Wildcard Routes

`Redirecting` mozeme pouzivat napriklad v pripade, ze uzivatel chce vyhladat udaj, ktory neexistuje. Pre take pripady sme si vytvorili komponentu PageNotFoundComponent, kde sa
zobrazi text s informaciou, ze stranka sa nenasla. V okamihu, ked uzivatel chce vyhladat neexistujuci vyraz presmeruje ho to na spominanu stranku. Na taketo presmerovanie sa pouziva `redirectTo: 'cestaKdanejStrankeKamChcemeBytPresmerovani'`

(subor app.module.ts)

```tsx
import { PageNotFoundComponent } from
'./page-not-found/page-not-found.component' const appRoutes: Routes = [ { path:
'', component: HomeComponent, pathMatch: 'full' }, { path: 'not-found',
component: PageNotFoundComponent }, { path: 'something', redirectTo:
'/not-found' }, ]
```

(subor page-not-found.component.html)

```html
<h3>This page was not found!</h3>
```

Zachytit uplne vsetky mozne vyrazy, ktore by uzivatelia chceli vyhladat nie je mozne, a preto sa pre taketo pripady vyuziva `Wildcard`. `WILDCARD MUSI BYT UVEDENA VZDY AKO POSLEDNA!`

(subor app.module.ts)

```tsx
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'

const appRoutes: Routes = [
	{ path: '', component: HomeComponent, pathMatch: 'full' },
	{ path: 'not-found', component: PageNotFoundComponent },
	{ path: '**', redirectTo: '/not-found' },
]
```

### pathMatch

`pathMatch` sa vyuziva na to, aby nam Angular nasiel len presne tu cestu, ktoru sme definovali. Tym
zabranime aby nasiel aj ine cesty, ktore obsahuju len cast z nasej cesty.

```tsx
{ path: '', redirectTo: '/somewhere-else', pathMatch: 'full' }
```

### Outsourcing route konfiguracie

Ak mame routov viac, mozme ich odobrat zo suboru app.module.ts a vytvorime si na to specialny subor
`app-routing.module.ts`. Aby data z nasho suboru boli dostupne pre nas hlavny route, vyuzivame `export`, v ktorom zadefinujeme co ma byt v hlavnom route dostupne.

(subor app-routing.module.ts)

```tsx
import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { HomeComponent } from './home/home.component'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { EditServerComponent } from './servers/edit-server/edit-server.component'
import { ServerComponent } from './servers/server/server.component'
import { ServersComponent } from './servers/servers.component'
import { UserComponent } from './users/user/user.component'
import { UsersComponent } from './users/users.component'

const appRoutes: Routes = [
	{ path: '', component: HomeComponent, pathMatch: 'full' },
	{
		path: 'users',
		component: UsersComponent,
		children: [{ path: ':id/:name', component: UserComponent }],
	},
	{
		path: 'servers',
		component: ServersComponent,
		children: [
			{ path: ':id', component: ServerComponent },
			{ path: ':id/edit', component: EditServerComponent },
		],
	},
	{ path: 'not-found', component: PageNotFoundComponent },
	{ path: '**', redirectTo: '/not-found' },
]
@NgModule({
	imports: [RouterModule.forRoot(appRoutes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
```

Importneme si subor app-routing.module.ts do app.module.ts

(subor app.module.ts)

```tsx
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module' // import

import { AppComponent } from './app.component'
import { HomeComponent } from './home/home.component'
import { UsersComponent } from './users/users.component'
import { ServersComponent } from './servers/servers.component'
import { UserComponent } from './users/user/user.component'
import { EditServerComponent } from './servers/edit-server/edit-server.component'
import { ServerComponent } from './servers/server/server.component'
import { ServersService } from './servers/servers.service'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		UsersComponent,
		ServersComponent,
		UserComponent,
		EditServerComponent,
		ServerComponent,
		PageNotFoundComponent,
	],
	imports: [BrowserModule, FormsModule, AppRoutingModule], //pridame do importu
	providers: [ServersService],
	bootstrap: [AppComponent],
})
export class AppModule {}
```

### canActivate

Sluzi na ochranu routov. `canActivate` nam umoznuje zbehnutie kodu v nami urcenom case. Vytvorime si
subor `auth-guard.service.ts`, v ktorom vyuzivame interface `CanActivate` od Angularu. CanActivate interface nam umoznuje vyuzit metodu `canActivate`, ktora bude mat dva argumenty.canActivate metoda nam bude vracat bud `Observable<boolean >` alebo `Promise<boolean >` ak pojde o asynchronnu funkciu alebo `boolean` ak pojde o synchronnu funkciu.

(subor auth-guard.service.ts)

```tsx
import {
	RouterStateSnapshot,
	ActivatedRouteSnapshot,
	CanActivate,
} from '@angular/router'
import { Observable } from 'rxjs/Observable'

export class AuthGuard implements CanActivate {
	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean> | Promise<boolean> | boolean {}
}
```

Vytvorime si dalsi subor na simulovanie
asynchronneho volania

(subor auth.service.ts )

```tsx
export class AuthService {
	loggedIn = false

	isAuthenticated() {
		const promise = new Promise((resolve, reject) => {
			setTimeout(() => {
				resolve(this.loggedIn)
			}, 800)
		})
		return promise
	}

	login() {
		this.loggedIn = true
	}
	logout() {
		this.loggedIn = false
	}
}
```

Do app.module.ts musime do `providers` uviest tieto vytvorene subory (AuthService, AuthGuard), aby o nich Angular vedel

```tsx
providers: [ServersService, AuthService, AuthGuard],
```

Do app-routing.module.ts pridavam do routy vlastnost `canActivate`, cim hovorim Angularu, aby AuthGuard pouzival. Takze server a jeho child routy budu dostupne iba vtedy ked canActivate metoda v AuthGuard vrati true, co sa stane iba vtedy ked v AuthService loggedIn bude nastavena ako true. Tato vlastnost sa bude vztahovat aj na route deti.

```tsx
{ path: "servers", canActivate: [AuthGuard], component: ServersComponent, children: [ { path: ":id", component: ServerComponent }, { path: ":id/edit", component: EditServerComponent }, ], },
```

#### canActivate child routes

Ak chceme mat zobrazenu hlavnu komponentu a rozhodovat iba o routingu jej deti, vyuzivame na to interface `CanActivateChild` od Angularu.

```tsx
import { CanActivate, CanActivateChild } from '@angular/router' export class AuthGuard implements CanActivate, CanActivateChild {}
```

Interface CanActivateChild ocakava vyuzivanie metody `canActivateChild()`, ktora ocakava dva argumenty - route a state presne tak, ako metoda canActivate().

(subor auth-guard.service.ts)

```tsx
import { Injectable } from '@angular/core'
import {
	RouterStateSnapshot,
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	CanActivateChild,
} from '@angular/router'
import { Observable } from 'rxjs/Observable'
import { AuthService } from './auth.service'

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
	constructor(private authService: AuthService, private router: Router) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean> | Promise<boolean> | boolean {
		return this.authService.isAuthenticated().then((authenticated: boolean) => {
			if (authenticated) {
				return true
			} else {
				this.router.navigate(['/'])
			}
		})
	}

	canActivateChild(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean> | Promise<boolean> | boolean {
		return this.canActivate(route, state)
	}
}
```

(zo suboru app-routing.module.ts)

```tsx
  {
    path: "servers",
    // canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: ServersComponent,
    children: [
      { path: ":id", component: ServerComponent },
      { path: ":id/edit", component: EditServerComponent },
    ],
  }
```

### Kontrola navigacie pomocou canDeactivate

`canDeactivate` kontroluje ci mame povolenie opustit route, na ktorom sa aktualne nachadzame.
Taketo spravanie moze byt pozadovane napriklad ked menime udaje na stranke a omylom klikneme na tlacitko `spat` v prehliadaci este predtym ako si ulozime nove data. Ako kazdy guard aj canDeactivate bude `service subor`. Najprv si zadefinujeme interface, ktory budu moct vyuzivat ostatne classy a tento interface ich bude nutit pouzivat zadefinovanu logiku. V interface nie je uvedena konkretna logika, iba to, ako ma logika vyzerat a teda, ze tam bude metoda `canDeactivate()` a ze jej vysledok bude typu `Observable<boolean >` alebo `Promise<boolean >` alebo `boolean`.
(zo suboru can-deactivate-guard.service.ts)

```tsx
import { Observable } from 'rxjs/Observable'

export interface CanComponentDeactivate {
	canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean
}
```

(subor can-deactivate-guard.service.ts)

```tsx
import {
	ActivatedRouteSnapshot,
	CanDeactivate,
	RouterStateSnapshot,
} from '@angular/router'
import { Observable } from 'rxjs/Observable'

export interface CanComponentDeactivate {
	canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean
}

//CanDeactivateGuard classa pouziva interface CanDeactivate od Angularu a je typu CanComponentDeactivate, co je nas vlastny interface, ktory ocakava canDeactivate metodu, ktoru sme si nadefinovali. CanDeactivateGuard class ocakava v sebe canDeactivate metodu. Angular bude tuto metodu volat, ked budeme chciet odist z komponenty.
export class CanDeactivateGuard
	implements CanDeactivate<CanComponentDeactivate> {
	canDeactivate(
		component: CanComponentDeactivate,
		currentRoute: ActivatedRouteSnapshot,
		currentState: RouterStateSnapshot,
		nextState?: RouterStateSnapshot
	): Observable<boolean> | Promise<boolean> | boolean {
		return component.canDeactivate()
	}
}
```

Do routov v subore app-routing.module.ts musime do daneho objektu zapisat tuto premennu, aby o nej Angular vedel. Angular bude volat tento guard vzdy ked budeme chciet opustit danu komponentu.

(zo suboru app-routing.module.ts)

```tsx
  {
    path: "servers",
    // canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: ServersComponent,
    children: [
      { path: ":id", component: ServerComponent },
      {
        path: ":id/edit",
        component: EditServerComponent,
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
```

Na to, aby nam to fungovalo, musime do suboru app.module.ts zapisat guard do providerov.
(subor app.module.ts)

```tsx
import { CanDeactivateGuard } from './servers/edit-server/can-deativate-guard.service'

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		UsersComponent,
		ServersComponent,
		UserComponent,
		EditServerComponent,
		ServerComponent,
		PageNotFoundComponent,
	],
	imports: [BrowserModule, FormsModule, AppRoutingModule],
	providers: [ServersService, AuthService, AuthGuard, CanDeactivateGuard],
	bootstrap: [AppComponent],
})
export class AppModule {}
```

Ostava nam este implementovat guard interface do komponenty, kde chceme aby bol nas guard dostupny

(zo suboru edit-server.component.ts)

```tsx
export class EditServerComponent implements OnInit, CanComponentDeactivate {}
```

(subor edit-server.component.ts)

```tsx
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params, Router } from '@angular/router'
import { Observable } from 'rxjs/Observable'

import { ServersService } from '../servers.service'
import { CanComponentDeactivate } from './can-deativate-guard.service'

@Component({
	selector: 'app-edit-server',
	templateUrl: './edit-server.component.html',
	styleUrls: ['./edit-server.component.css'],
})
export class EditServerComponent implements OnInit, CanComponentDeactivate {
	server: { id: number; name: string; status: string }
	serverName = ''
	serverStatus = ''
	allowEdit = false
	changesSaved = false

	constructor(
		private serversService: ServersService,
		private route: ActivatedRoute,
		private router: Router
	) {}

	ngOnInit() {
		console.log(this.route.snapshot.queryParams)
		console.log(this.route.snapshot.fragment)
		this.route.queryParams.subscribe((queryParams: Params) => {
			this.allowEdit = queryParams['allowEdit'] === '1' ? true : false
		})
		this.route.fragment.subscribe()
		const id = +this.route.snapshot.params['id']
		this.server = this.serversService.getServer(id)
		// Subscribe route params to update the id of params change
		this.serverName = this.server.name
		this.serverStatus = this.server.status
	}

	onUpdateServer() {
		this.serversService.updateServer(this.server.id, {
			name: this.serverName,
			status: this.serverStatus,
		})
		this.changesSaved = true
		this.router.navigate(['../'], { relativeTo: this.route })
	}

	//logika, kde sa rozhoduje ci mame dovolene opustit stranku alebo nemame, tato metoda zbehne vzdy ked je nas guard skontrolovany Angular routom.
	canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
		if (!this.allowEdit) {
			return true
		}
		if (
			(this.serverName !== this.server.name ||
				this.serverStatus !== this.server.status) &&
			!this.changesSaved
		) {
			return confirm('Do you want to discard the changes?')
		} else {
			return true
		}
	}
}
```

### Presun statickych dat do routu

Vytvorili sme si novu komponentu `ErrorPageComponent`, ktoru budeme vyuzivat na zobrazenie errorovej
spravy v pripade, ze uzivatel bude chciet vyhladat neexistujucu stranku.

(subor error-page.component.html)

```html
<h4>{{ errorMessage }}</h4>
```

(subor error-page.component.ts)

```tsx
@Component({
	selector: 'app-error-page',
	templateUrl: './error-page.component.html',
	styleUrls: ['./error-page.component.css'],
})
export class ErrorPageComponent implements OnInit {
	errorMessage: string

	constructor(private route: ActivatedRoute) {}

	// ak by sa data zmenili pocas toho ako sme na stranke, treba pouzit subscribe namiesto snapshot, aby sme mali dostupne aktualne data. Ak sa vsak nic menit nebude, je v poriadku pouzit snapshot pre ziskanie dat
	ngOnInit(): void {
		// this.errorMessage = this.route.snapshot.data['message']
		this.route.data.subscribe((data: Data) => {
			this.errorMessage = data['message']
		})
	}
}
```

Do routy pridavame novu premennu `ErrorPageComponent` a tiez `data`, ktore obsahuju `message`, ktora sa zobrazi, ak uzivatel bude vyhladavat neexistujucu stranku. Text spravy sa neuvadzal priamo do `ErrorPageComponent`, pretoze tuto komponentu chceme vyuzivat pri viacerych pripadoch a teda obsah spravy sa bude menit v zavyslosti od pripadu.

(zo suboru app-routing.module.ts)

```tsx
{ path: "not-found", component: ErrorPageComponent, data: { message: "Page not found!" }, },
```

### Riesenie dynamickych dat pomocou resolve Guardu

Resolve Guard je tak ako vsetky guardy servis. Umoznuje nam zbehnutie kodu pred tym ako je route vykresleny. Rozdiel medzi nim a guardom canActivate je, ze resolver sa nebude rozhodovat ci route moze alebo nemoze byt vykresleny (ci komponenta moze byt nacitana alebo nemoze). Resolver vzdy vykresli komponentu ale tiez bude robit prednacitanie. Vytvorili sme si `ServerResolver` service, ktory implementuje interface `Resolve` od Angularu. Class ServerResolver ocakava v sebe metodu
`resolve()`, ktora ma dva vstupne parametre `route` a `state`.

(subor server-resolver.service.ts)

```tsx
import {
	ActivatedRouteSnapshot,
	Resolve,
	RouterStateSnapshot,
} from '@angular/router'
import { Observable } from 'rxjs/Observable'

interface Server {
	id: number
	name: string
	status: string
}

export class ServerResolver
	implements Resolve<{ id: number; name: string; status: string }> {
	resolve(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<Server> | Promise<Server> | Server {}
}
```

Resolver je potrebne pridat do app.module.ts do pola `providers`

(zo suboru app.module.ts)

```tsx
  providers: [
    ServersService,
    AuthService,
    AuthGuard,
    CanDeactivateGuard,
    ServerResolver,
  ],
```

Resolver sa pridava do routu, kde ho chceme implementovat v trochu inom tvare ako ostatne guardy. Davame ho do premennej `resolve` a ta nebude pole (ako pri ostatnych guardoch) ale bude to objekt.

(zo suboru app-routing.module.ts)

```tsx
 {
    path: "servers",
    // canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: ServersComponent,
    children: [
      { path: ":id", component: ServerComponent, resolve: {server: ServerResolver} },
      {
        path: ":id/edit",
        component: EditServerComponent,
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
```

Funkciu `ngOnInit()` menime, nakolko uz vieme vyuzit resolver.

(subor server.component.ts)

```tsx
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Data, Params, Router } from '@angular/router'

import { ServersService } from '../servers.service'

@Component({
	selector: 'app-server',
	templateUrl: './server.component.html',
	styleUrls: ['./server.component.css'],
})
export class ServerComponent implements OnInit {
	server: { id: number; name: string; status: string }

	constructor(
		private serversService: ServersService,
		private route: ActivatedRoute,
		private router: Router
	) {}

	ngOnInit() {
		this.route.data.subscribe((data: Data) => {
			this.server = data['server'] // nazov v zatvorkach sa musi rovnat nazvu aky sme pouzili v subore app-routing.module.ts v resolve objekte !!!!!
		})
		// const id = +this.route.snapshot.params["id"];
		// this.server = this.serversService.getServer(id);
		// this.route.params.subscribe((params: Params) => {
		//   this.server = this.serversService.getServer(+params["id"]);
		// });
	}

	onEdit() {
		this.router.navigate(['edit'], {
			relativeTo: this.route,
			queryParamsHandling: 'preserve',
		})
	}
}
```

#### Riesenie vykreslenia routov pri starsich browsroch pomocou useHash

Mozme pouzit starsiu techniku pouzivanu pred rokmi. Do `forRoot()` metody uvedieme dalsi argument, bude to objekt, kde `useHash` bude `true` (defaltne je false). `#` hovori web serveru, aby sa staral len o url uvedenou pred `#`, cela cast url za `#` bude ignorovana. Takze toto nam zbehne aj na servroch, ktore nevracaju index.html subor v pripade `eroru 404`. Cast url za `#` tak zbehne vdaka Angularu

(zo suboru app-routing.module.ts)

```tsx
@NgModule({ imports: [RouterModule.forRoot(appRoutes, { useHash: true })], exports: [RouterModule], })
```

### Observable

Su to rozne zdroje dat, ako inputy od usera, eventy, http requesty,... v projekte od udemy si observable mozme predstavit ako objekt importovany od tretej strany - od rxjs. Observable maju
urcity patern - mame observable a tiez observera, medzi nimi je urcita casova os. Na tejto casovej
osy mozme mat niekolko eventov emitovanych pomocou observable v zavislosti od zdroja dat. Data sa
budu emitovat pretoze sme vyvolali akciu kliknutim na gombik, alebo napriklad pomocou http requestu na servis.

Pracujeme s 3 typy balickov dat:

- `handle data` - pracovanie s normalnymi datami
- `handle error` - pracovanie s errormi
- `handle completion` - pracovanie s dokoncenim/uzatvaranim observables

Moze sa stat, ze `nie vsetky observable budu ukoncene`.

### interval(), unsubscribe(), onDestroy, ngOnDestroy - zastavenie nezelanej pamatovej slucky

Ak vyuzivame observable od Angularu, napriklad `params` nemusime pouzit metodu `unsubscribe()`, Angular to osetri za nas, ak vsak pouzijeme observable od rxjs, alebo nasu vlastnu, musime vyuzit
`unsubscribe()`, aby sme sa vyhli nezelanemu spravaniu kodu a chybam.

(subor home.component.ts)

```tsx
import { Component, OnDestroy, OnInit } from '@angular/core'
import { interval, Subscription } from 'rxjs'

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
	//potrebujeme premennu na ulozenie nasho intervalu(nasej observable), aby sme nasledne vedeli pouzit metodu unsubscribe na zastavenie eventu
	private firstObsSubscription: Subscription

	constructor() {}

	//interval vytvori event, ktory nam vrati novu hodnotu kazdu sekundu
	ngOnInit() {
		this.firstObsSubscription = interval(1000).subscribe(count => {
			console.log(count)
		})
	}

	//pomocou unsubscribe() zastavime interval, ktory nam kazdu sekundu vrati novu hodnotu. Inak by tento event stale bezal, vytvorila by sa pamatova slucka, tym by sa nasa aplikacia spomalila.
	ngOnDestroy() {
		this.firstObsSubscription.unsubscribe()
	}
}
```

### budovanie vlastnej observable

Na budovanie vlastnej observable si potrebujeme importnut Observable z balicka rxjs

```tsx
import { interval, Subscription, Observable } from 'rxjs'
```

(subor home.component.ts)

```tsx
import { Component, OnDestroy, OnInit } from '@angular/core'
import { interval, Subscription, Observable } from 'rxjs'

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
	private firstObsSubscription: Subscription

	constructor() {}

	ngOnInit() {
		//rxjs observable, to iste co v custom observable
		// this.firstObsSubscription = interval(1000).subscribe((count) => {
		//   console.log(count);
		// });

		//custom observable
		const customIntervalObservable = Observable.create(observer => {
			let count = 0
			setInterval(() => {
				observer.next(count)
				count++
			}, 1000)
		})

		this.firstObsSubscription = customIntervalObservable.subscribe(data => {
			console.log(data)
		})
	}

	ngOnDestroy() {
		this.firstObsSubscription.unsubscribe()
	}
}
```

#### Metoda volana pri tvoreni custom observable - create()

- `create(() => {})` - sluzi na vytvorenie observable

#### Metody observera

- `observer.next()` - next volame na emitovanie hodnoty
- `observer.error()` - error sluzi na hodenie erroru
- `observer.complete()` - complete oboznami observera, ze sme skoncili

### Errors & Completion

(subor home.cmponent.ts)

```tsx
import { Component, OnDestroy, OnInit } from '@angular/core'
import { interval, Subscription, Observable } from 'rxjs'

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
	private firstObsSubscription: Subscription

	constructor() {}

	ngOnInit() {
		//custom observable
		const customIntervalObservable = Observable.create(observer => {
			let count = 0
			setInterval(() => {
				observer.next(count)
				if (count === 2) {
					observer.complete()
				}
				if (count > 3) {
					observer.error(new Error('Count is greater 3!'))
				}
				count++
			}, 1000)
		})

		// Do subscribe() vieme dat niekolko argumentov, prvy je na data, druhy na errory a treti na dokoncenie
		this.firstObsSubscription = customIntervalObservable.subscribe(
			data => {
				console.log(data)
			},
			error => {
				console.log(error)
				alert(error.message)
			},
			() => {
				console.log('Completed!')
			}
		)
	}

	ngOnDestroy() {
		this.firstObsSubscription.unsubscribe()
	}
}
```

Mame tu situaciu, ze error() zbehne skor ako complete(), pretoze tak sme si nastavili podmienky. V takom pripade nasa complete() metoda sa nezavola, pretoze najprv zbehne error() a tam sa nam kod zrusi. Zrusenie kodu ale neznamena, ze je dokonceny, tak ako ked nam zbehne complete() metoda!

```tsx
	ngOnInit() {
		//custom observable
		const customIntervalObservable = Observable.create(observer => {
			let count = 0
			setInterval(() => {
				observer.next(count)
				if (count === 5) {
					observer.complete()
				}
				if (count > 3) {
					observer.error(new Error('Count is greater 3!'))
				}
				count++
			}, 1000)
		})

		// Do subscribe() vieme dat niekolko argumentov, prvy je na data, druhy na errory a treti na dokoncenie
		this.firstObsSubscription = customIntervalObservable.subscribe(
			data => {
				console.log(data)
			},
			error => {
				console.log(error)
				alert(error.message)
			},
			() => {
				console.log('Completed!')
			}
		)
```

### Operators, pipe()

Operatori pochadzaju z balicka rxjs. Mozme si predstavit, ze su to take medziclanky medzi observables a observerom. Ak by sme chceli, aby nam console.log() vypisal text "Round: 1...Round:2. ." atd. vieme to urobit aj takto. Pre jednoduche operacie ako je tato je aj tento sposob zapisu kodu v poriadku.

(zo suboru home.component.ts)

```tsx
this.firstObsSubscription = customIntervalObservable.subscribe(
        (data) => {
          console.log('Round: ' + (data + 1)); //TU
        },
        (error) => {
          console.log(error);
          alert(error.message);
        },
        () => {
          console.log("Completed!");
        }
      );
  }
```

Ale cim komplexnejsia nasa logika bude, tym krajsie bude pisat ju nie na miesto kde pouzivame subscribe(), ale na miesto este pred pouzitim subscribe(). Co ak by sme chceli menit logiku napriklad v `params` (co su observable od Angularu a teda ich "nevlastnime", nevytvorili sme si ich samy). Nemozme menit ich original kod. Kvoli takymto pripadom tu mame operatori, mozme ich pouzit na
vsetky observable pomocou metody `pipe()`. Kazda observable ma pipe() metodu. Pipe() metoda moze
mat v sebe `neobmedzene mnozstvo argumentov (operatorov)`.

(subor home.component.ts)

```tsx
import { Component, OnDestroy, OnInit } from '@angular/core'
import { Subscription, Observable } from 'rxjs'
import { map, filter } from 'rxjs/operators' //OPERATORI

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
	private firstObsSubscription: Subscription

	constructor() {}

	ngOnInit() {
		//custom observable
		const customIntervalObservable = Observable.create(observer => {
			let count = 0
			setInterval(() => {
				observer.next(count)
				if (count == 2) {
					observer.complete()
				}
				if (count > 3) {
					observer.error(new Error('Count is greater 3!'))
				}
				count++
			}, 1000)
		})
		//do premennej nesmieme zabudnut vlozit customIntervalObservable.pipe(...), aby sme pracovali s najnovsimi datami
		this.firstObsSubscription = customIntervalObservable
			.pipe(
				filter(data => {
					return data > 0
				}),
				map((data: number) => {
					return 'Round: ' + (data + 1)
				})
			)
			.subscribe(
				data => {
					console.log(data)
				},
				error => {
					console.log(error)
					alert(error.message)
				},
				() => {
					console.log('Completed!')
				}
			)
	}

	ngOnDestroy() {
		this.firstObsSubscription.unsubscribe()
	}
}
```

### Subject

Importujeme ho z rxjs.

```tsx
import { Subscription } from 'rxjs'
```

Vyuzivame ho `namiesto EventEmitteru`. Tak isto ako EventEmitteru, aj Subjectu sa zapisuje typescript do `<...>`.

(subor user.service.ts)

```tsx
import { Injectable } from '@angular/core'
import { Subject } from 'rxjs'

@Injectable({ providedIn: 'root' })
export class UserService {
	activatedEmitter = new Subject<boolean>()
}
```

Na rozdiel od EventEmitteru nebudeme pri Subjecte vyuzivat metodu emit(), ale `next()`, pretoze `Subject je specialny typ observable`.

(zo suboru user.component.ts)

```tsx
 onActivate() {
    this.userService.activatedEmitter.next(true);
  }
```

Pretoze Subject je observable, mozme dalej vyuzivat subscribe() metodu.

(zo suboru app.component.ts)

```tsx
ngOnInit() {
    this.activatedSub = this.userService.activatedEmitter.subscribe(
      (didActivate) => {
        this.userActivated = didActivate;
      }
    );
  }
```

Nesmieme zabudnut pouzit `unsubscribe()` metodu ked uz Subjecty nebudeme potrebovat.

(zo suboru app.component.ts)

```tsx
 ngOnDestroy() {
    this.activatedSub.unsubscribe();
  }
```

Odporuca sa ulozit si subscription do nejakej premennej, aby sme potom mohli vyuzit unsubscribe() metodu, napriklad:

```tsx
import { Subscription } from "rxjs";

private activatedSub: Subscription

ngOnInit() {
    this.activatedSub = this.userService.activatedEmitter.subscribe(
      (didActivate) => {
        this.userActivated = didActivate;
      }
    );
  }

	 ngOnDestroy() {
    this.activatedSub.unsubscribe();
  }
```

Subjecty pouzivame v pripade, ze ich vyuzivame ako medzikomponentovy emitter. Pouzivame ich na komunikaciu medzi komponentami pomocou servisov, manualne volame metodu subscribe(). Nepouzivame ich pri @Output-och. Tam stale budeme vyuzivat angularovsky EventEmitter. Ak nepotrebujeme vyuzit metodu subscribe() tak sa bude jednat o @Output, kde teda budeme mat EventEmitter.

#### Uzitocne linky

Official Docs: https://rxjs-dev.firebaseapp.com/
RxJS Series: https://academind.com/learn/javascript/understanding-rxjs/
Updating to RxJS 6: https://academind.com/learn/javascript/rxjs-6-what-changed/

### LifeCycle Hooks

- `ngOnChanges` metoda je zavolana hned na zaciatku ked je vytvorena komponenta a potom vzdy ked sa nejaka napojena input premenna zmeni (premenne, ktore su cez dekorator @Input zasielana do komponenty)
- `ngOnInit` metoda sa vola len raz pri nacitani komponenty (t.j. ked su nase premenne dostupne, ked je objekt vytvoreny). V kode najprv zbehne konstruktor(ak ho mame) a potom tato metoda
- `ngDoCheck` metoda je zavolana vzdy, ked sa spusti `change detection system` (pomocou tohto systemu Angular zistuje, ci v template danej komponenty nastala nejaka zmena). Tato metoda nezbehne len ak nastane nejaka zmena, zbehne vzdy, ked sa udeje nejaky event (aj keby sa tymto eventom
  nezmenilo v kode nic)
- `ngAfterContentInit` metoda je zavolana po zobrazeni contentu (`ng-content`) v rodicovskej
  komponente
- `ngAfterContentChecked` metoda je zavolana vzdy, ked sa spusti change detection system na danom
  contente
- `ngAfterViewInit` metoda je zavolana po tom, co sa rodicovska komponenta (a aj komponenta dietata)
  nainicializuje. Ked sa vykresli na stranke.
- `ngAfterViewChecked` metoda je zavolana vzdy, ked sa spusti change detection system na danej komponente
- `ngOnDestroy` metoda je zavolana, ked sa komponenta vymaze

<!-- </boolean
    																						></Server
    																					></Server
    																				></boolean
    																			></boolean
    																		></boolean
    																	></boolean
    																></CanComponentDeactivate
    															></boolean
    														></boolean
    													></boolean
    												></boolean
    											></boolean
    										></boolean
    									></boolean
    								></boolean
    							></boolean
    						></boolean
    					></boolean
    				></boolean
    			></boolean
    		></boolean
    	></ValidationErrors

> </ValidationErrors

>

```

```

```

```

```

```

```

```

```

```

```

``` -->
